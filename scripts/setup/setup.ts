import { PrismaHideUserPasswordExtension } from "@prisma-extensions";
import { PrismaClient } from "@prisma/client";

import * as process from "process";
import * as dotenv from "dotenv";

import { hash } from "argon2";

import { TD } from "@td";

dotenv.config();

const SERVICE_USERNAME = process.env["TD_SERVICE_USERNAME"];
const SERVICE_PASSWORD = process.env["TD_SERVICE_PASSWORD"];

if (!SERVICE_USERNAME || !SERVICE_PASSWORD) {
	console.error(
		">>> No service account username or password supplied. Will not proceed with setup.",
	);

	process.exit(1);
}

const prisma = new PrismaClient().$extends(PrismaHideUserPasswordExtension);

async function main() {
	await prisma.$connect();

	const u = await prisma.user.create({
		data: {
			last_name: "Service",
			first_name: "Account",
			username: SERVICE_USERNAME,
			password: await hash(SERVICE_PASSWORD),
			role: TD.Types.UserRole.ADMIN,
			is_registered: true,
		},
	});

	console.log(">>> Created new service user:", u);

	await prisma.$disconnect();
	process.exit();
}

main();
