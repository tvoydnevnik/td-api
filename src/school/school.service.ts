import { Injectable } from "@nestjs/common";

import { from, Observable } from "rxjs";

import { PrismaService } from "@td-prisma";

import { TD } from "@td";

@Injectable()
export class SchoolService {
	constructor(private readonly prisma: PrismaService) {}

	GetSchoolInfo(): Observable<TD.Types.ISchool> {
		return from(
			this.prisma.school.findUnique({
				where: { id: 1 },
				include: { headmaster: { include: { user: true } } },
			}),
		) as Observable<TD.Types.ISchool>;
	}

	UpdateSchoolInfo(
		payload: TD.Types.DeepPartial<TD.Types.ISchool>,
	): Observable<TD.Types.ISchool> {
		return from(
			this.prisma.school.update({
				where: { id: 1 },
				data: {
					name: payload.name,
					address: payload.address,
					contacts: payload.contacts,
					headmaster_id:
						payload.headmaster === null ? null : payload.headmaster?.id,
				},
			}),
		) as Observable<TD.Types.ISchool>;
	}

	GetBellSchedules(): Observable<TD.Types.IBellsSchedule[]> {
		return from(this.prisma.bellsSchedule.findMany()) as Observable<
			TD.Types.IBellsSchedule[]
		>;
	}

	GetBellsSchedule(id: number): Observable<TD.Types.IBellsSchedule> {
		return from(
			this.prisma.bellsSchedule.findUnique({ where: { id } }),
		) as Observable<TD.Types.IBellsSchedule>;
	}

	AddBellsSchedule(
		payload: TD.Types.IBellsSchedulePayload,
	): Observable<TD.Types.IBellsSchedule> {
		return from(
			this.prisma.bellsSchedule.create({
				data: {
					name: payload.name,
					schedule: payload.schedule,
				},
			}),
		) as Observable<TD.Types.IBellsSchedule>;
	}

	UpdateBellsSchedule(
		id: number,
		payload: TD.Types.IBellsSchedulePayload,
	): Observable<TD.Types.IBellsSchedule> {
		return from(
			this.prisma.bellsSchedule.update({
				where: { id },
				data: {
					name: payload.name,
					schedule: payload.schedule,
				},
			}),
		) as Observable<TD.Types.IBellsSchedule>;
	}

	DeleteBellsSchedule(id: number): Observable<TD.Types.IBellsSchedule> {
		return from(
			this.prisma.bellsSchedule.delete({ where: { id } }),
		) as Observable<TD.Types.IBellsSchedule>;
	}
}
