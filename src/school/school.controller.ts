import {
	Body,
	Controller,
	Delete,
	Get,
	Param,
	Patch,
	Post,
} from "@nestjs/common";

import { mergeMap } from "rxjs";

import { SchoolService } from "./school.service";

import { TD } from "@td";

@Controller("school")
export class SchoolController {
	constructor(private readonly schoolService: SchoolService) {}

	@Get("about")
	GetInfo(): TD.EndpointResponse<TD.Types.ISchool> {
		return this.schoolService
			.GetSchoolInfo()
			.pipe(mergeMap((data) => TD.CreateEndpointResponse(data)));
	}

	@TD.MinimalRole(TD.Types.UserRole.ADMIN)
	@Patch("update")
	UpdateInfo(
		@Body() body: Partial<TD.Types.ISchool>,
	): TD.EndpointResponse<TD.Types.ISchool> {
		return this.schoolService
			.UpdateSchoolInfo(body)
			.pipe(mergeMap((data) => TD.CreateEndpointResponse(data)));
	}

	@Get("bell-schedules")
	GetBellSchedules(): TD.EndpointResponse<TD.Types.IBellsSchedule[]> {
		return this.schoolService
			.GetBellSchedules()
			.pipe(mergeMap((data) => TD.CreateEndpointResponse(data)));
	}

	@Get("bells-schedule/:id")
	GetBellsSchedule(
		@Param("id") id: number,
	): TD.EndpointResponse<TD.Types.IBellsSchedule> {
		return this.schoolService
			.GetBellsSchedule(id)
			.pipe(mergeMap((data) => TD.CreateEndpointResponse(data)));
	}

	@TD.MinimalRole(TD.Types.UserRole.TEACHER)
	@Post("bells-schedule")
	AddBellsSchedule(
		@Body() body: TD.Types.IBellsSchedulePayload,
	): TD.EndpointResponse<TD.Types.IBellsSchedule> {
		return this.schoolService
			.AddBellsSchedule(body)
			.pipe(mergeMap((data) => TD.CreateEndpointResponse(data)));
	}

	@TD.MinimalRole(TD.Types.UserRole.TEACHER)
	@Patch("bells-schedule/:id")
	UpdateBellsSchedule(
		@Param("id") id: number,
		@Body() body: TD.Types.IBellsSchedulePayload,
	): TD.EndpointResponse<TD.Types.IBellsSchedule> {
		return this.schoolService
			.UpdateBellsSchedule(id, body)
			.pipe(mergeMap((data) => TD.CreateEndpointResponse(data)));
	}

	@TD.MinimalRole(TD.Types.UserRole.TEACHER)
	@Delete("bells-schedule/:id")
	DeleteBellsSchedule(
		@Param("id") id: number,
	): TD.EndpointResponse<TD.Types.IBellsSchedule> {
		return this.schoolService
			.DeleteBellsSchedule(id)
			.pipe(mergeMap((data) => TD.CreateEndpointResponse(data)));
	}
}
