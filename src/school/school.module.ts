import { SchoolController } from "./school.controller";
import { SchoolService } from "./school.service";
import { Module } from "@nestjs/common";

@Module({
	controllers: [SchoolController],
	providers: [SchoolService],
})
export class SchoolModule {}
