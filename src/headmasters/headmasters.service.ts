import { Injectable } from "@nestjs/common";
import { PrismaService } from "@td-prisma";
import { from, Observable } from "rxjs";
import { TD } from "@td";

@Injectable()
export class HeadmastersService {
	constructor(private readonly prisma: PrismaService) {}

	GetAll(): Observable<TD.Types.IHeadmaster[]> {
		return from(
			this.prisma.headmaster.findMany({ include: { user: true } }),
		) as Observable<TD.Types.IHeadmaster[]>;
	}

	GetById(id: number): Observable<TD.Types.IHeadmaster> {
		return from(
			this.prisma.headmaster.findUnique({
				where: { id },
				include: { user: true },
			}),
		) as Observable<TD.Types.IHeadmaster>;
	}
}
