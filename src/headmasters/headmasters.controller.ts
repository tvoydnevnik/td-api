import { HeadmastersService } from "./headmasters.service";
import { Controller, Get, Param } from "@nestjs/common";
import { mergeMap } from "rxjs";
import { TD } from "@td";

@Controller("headmasters")
export class HeadmastersController {
	constructor(private readonly headmastersService: HeadmastersService) {}

	@Get()
	GetAll(): TD.EndpointResponse<TD.Types.IHeadmaster[]> {
		return this.headmastersService
			.GetAll()
			.pipe(mergeMap((r) => TD.CreateEndpointResponse(r)));
	}

	@Get(":id")
	GetById(@Param("id") id: number): TD.EndpointResponse<TD.Types.IHeadmaster> {
		return this.headmastersService
			.GetById(id)
			.pipe(mergeMap((r) => TD.CreateEndpointResponse(r)));
	}
}
