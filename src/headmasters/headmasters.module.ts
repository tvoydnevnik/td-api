import { HeadmastersController } from "./headmasters.controller";
import { HeadmastersService } from "./headmasters.service";
import { Module } from "@nestjs/common";

@Module({
	controllers: [HeadmastersController],
	providers: [HeadmastersService],
})
export class HeadmastersModule {}
