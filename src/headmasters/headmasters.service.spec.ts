import { Test, TestingModule } from "@nestjs/testing";
import { HeadmastersService } from "./headmasters.service";

describe("HeadmastersService", () => {
	let service: HeadmastersService;

	beforeEach(async () => {
		const module: TestingModule = await Test.createTestingModule({
			providers: [HeadmastersService],
		}).compile();

		service = module.get<HeadmastersService>(HeadmastersService);
	});

	it("should be defined", () => {
		expect(service).toBeDefined();
	});
});
