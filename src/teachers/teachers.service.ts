import { Injectable } from "@nestjs/common";

import { from, Observable, tap } from "rxjs";

import { PrismaService } from "@td-prisma";
import { Prisma } from "@prisma/client";

import { TD } from "@td";

@Injectable()
export class TeachersService {
	private readonly _relations: Prisma.TeacherInclude = {
		user: true,
		master_of: true,
	};

	constructor(private readonly prisma: PrismaService) {}

	GetAll(): Observable<TD.Types.ITeacher[]> {
		return from(
			this.prisma.teacher.findMany({
				include: this._relations,
			}),
		) as Observable<TD.Types.ITeacher[]>;
	}

	GetByUserId(user_id: number): Observable<TD.Types.ITeacher> {
		return from(
			this.prisma.teacher.findFirst({
				where: { user_id },
				include: this._relations,
			}),
		) as Observable<TD.Types.ITeacher>;
	}

	GetById(id: number): Observable<TD.Types.ITeacher> {
		return from(
			this.prisma.teacher.findUnique({
				where: { id },
				include: this._relations,
			}),
		).pipe(
			tap((v) => {
				if (!v)
					throw TD.CreateHttpException(
						TD.HTTP_STATUS.NOT_FOUND,
						"Teacher not found",
					);
			}),
		) as Observable<TD.Types.ITeacher>;
	}

	CreateOne(
		payload: TD.Types.DeepPartial<TD.Types.ITeacher>,
	): Observable<TD.Types.ITeacher> {
		return from(
			this.prisma.teacher.create({
				data: {
					user_id: payload.user?.id,
					duty: payload.duty,
					master_of: { connect: { id: payload.master_of?.id } },
				},
			}),
		);
	}

	DeleteOne(id: number): Observable<TD.Types.ITeacher> {
		return from(this.prisma.teacher.delete({ where: { id } }));
	}

	GetLessons(id: number): Observable<TD.Types.IFormLesson[]> {
		return from(
			this.prisma.teacher.findUnique({ where: { id } }).form_lessons({
				include: {
					lesson: true,
					form: true,
				},
			}),
		);
	}
}
