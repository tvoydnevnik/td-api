import { TeachersController } from "./teachers.controller";
import { TeachersService } from "./teachers.service";
import { Module } from "@nestjs/common";

@Module({
	controllers: [TeachersController],
	providers: [TeachersService],
})
export class TeachersModule {}
