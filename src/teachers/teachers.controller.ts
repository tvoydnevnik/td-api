import { Body, Controller, Delete, Get, Param, Post } from "@nestjs/common";
import { TeachersService } from "./teachers.service";
import { mergeMap } from "rxjs";
import { TD } from "@td";

@Controller("teachers")
export class TeachersController {
	constructor(private readonly teachersService: TeachersService) {}

	@Get()
	GetAll(): TD.EndpointResponse<TD.Types.ITeacher[]> {
		return this.teachersService
			.GetAll()
			.pipe(mergeMap((t) => TD.CreateEndpointResponse(t)));
	}

	@Get("by-user-id/:user_id")
	GetByUserId(
		@Param("user_id") user_id: number,
	): TD.EndpointResponse<TD.Types.ITeacher> {
		return this.teachersService
			.GetByUserId(user_id)
			.pipe(mergeMap((t) => TD.CreateEndpointResponse(t)));
	}

	@Get(":id")
	GetById(@Param("id") id: number): TD.EndpointResponse<TD.Types.ITeacher> {
		return this.teachersService
			.GetById(id)
			.pipe(mergeMap((t) => TD.CreateEndpointResponse(t)));
	}

	@Get(":id/lessons")
	GetTeacherLessons(
		@Param("id") id: number,
	): TD.EndpointResponse<TD.Types.IFormLesson[]> {
		return this.teachersService
			.GetLessons(id)
			.pipe(mergeMap((l) => TD.CreateEndpointResponse(l)));
	}

	@TD.MinimalRole(TD.Types.UserRole.ADMIN)
	@Post()
	CreateOne(
		@Body() body: TD.Types.ITeacher,
	): TD.EndpointResponse<TD.Types.ITeacher> {
		return this.teachersService
			.CreateOne(body)
			.pipe(mergeMap((t) => TD.CreateEndpointResponse(t)));
	}

	@TD.MinimalRole(TD.Types.UserRole.ADMIN)
	@Delete(":id")
	DeleteOne(@Param("id") id: number): TD.EndpointResponse<TD.Types.ITeacher> {
		return this.teachersService
			.DeleteOne(id)
			.pipe(mergeMap((res) => TD.CreateEndpointResponse(res)));
	}
}
