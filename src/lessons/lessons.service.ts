import { Injectable } from "@nestjs/common";

import { from, Observable, tap } from "rxjs";

import { PrismaService } from "@td-prisma";

import { TD } from "@td";

@Injectable()
export class LessonsService {
	constructor(private readonly prisma: PrismaService) {}

	GetAll(): Observable<TD.Types.ILesson[]> {
		return from(this.prisma.lesson.findMany());
	}

	GetById(id: number): Observable<TD.Types.ILesson> {
		return from(this.prisma.lesson.findUnique({ where: { id } })).pipe(
			tap((v) => {
				if (!v)
					throw TD.CreateHttpException(
						TD.HTTP_STATUS.NOT_FOUND,
						"Lesson not found",
					);
			}),
		);
	}

	CreateOne(
		payload: TD.Types.DeepPartial<TD.Types.ILesson>,
	): Observable<TD.Types.ILesson> {
		return from(this.prisma.lesson.create({ data: { name: payload.name } }));
	}

	UpdateOne(
		id: number,
		payload: TD.Types.DeepPartial<TD.Types.ILesson>,
	): Observable<TD.Types.ILesson> {
		return from(
			this.prisma.lesson.update({
				where: { id },
				data: { name: payload.name },
			}),
		);
	}

	DeleteOne(id: number): Observable<TD.Types.ILesson> {
		return from(this.prisma.lesson.delete({ where: { id } }));
	}
}
