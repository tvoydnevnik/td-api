import { LessonsController } from "./lessons.controller";
import { LessonsService } from "./lessons.service";
import { Module } from "@nestjs/common";

@Module({
	controllers: [LessonsController],
	providers: [LessonsService],
	exports: [LessonsService],
})
export class LessonsModule {}
