import { LessonsService } from "./lessons.service";
import { mergeMap } from "rxjs";
import {
	Body,
	Controller,
	Delete,
	Get,
	Param,
	Patch,
	Post,
} from "@nestjs/common";
import { TD } from "@td";

@Controller("lessons")
export class LessonsController {
	constructor(private readonly lessonsService: LessonsService) {}

	@Get()
	GetAll(): TD.EndpointResponse<TD.Types.ILesson[]> {
		return this.lessonsService
			.GetAll()
			.pipe(mergeMap((lessons) => TD.CreateEndpointResponse(lessons)));
	}

	@Get(":id")
	GetById(@Param("id") id: number): TD.EndpointResponse<TD.Types.ILesson> {
		return this.lessonsService
			.GetById(id)
			.pipe(mergeMap((lesson) => TD.CreateEndpointResponse(lesson)));
	}

	@TD.MinimalRole(TD.Types.UserRole.HEADMASTER)
	@Post()
	CreateOne(
		@Body() payload: TD.Types.ILesson,
	): TD.EndpointResponse<TD.Types.ILesson> {
		return this.lessonsService
			.CreateOne(payload)
			.pipe(mergeMap((lesson) => TD.CreateEndpointResponse(lesson)));
	}

	@TD.MinimalRole(TD.Types.UserRole.HEADMASTER)
	@Patch(":id")
	UpdateOne(
		@Param("id") id: number,
		@Body() payload: Partial<TD.Types.ILesson>,
	): TD.EndpointResponse<TD.Types.ILesson> {
		return this.lessonsService
			.UpdateOne(id, payload)
			.pipe(mergeMap((lesson) => TD.CreateEndpointResponse(lesson)));
	}

	@TD.MinimalRole(TD.Types.UserRole.HEADMASTER)
	@Delete(":id")
	DeleteOne(@Param("id") id: number): TD.EndpointResponse<TD.Types.ILesson> {
		return this.lessonsService
			.DeleteOne(id)
			.pipe(mergeMap((res) => TD.CreateEndpointResponse(res)));
	}
}
