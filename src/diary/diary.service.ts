import { Injectable } from "@nestjs/common";

import { from, map, Observable } from "rxjs";

import { PrismaService } from "@td-prisma";

import { TD } from "@td";

@Injectable()
export class DiaryService {
	constructor(private readonly prisma: PrismaService) {}

	GetDiary(
		request: TD.Types.IDiaryRequest,
	): Observable<TD.Types.IDiaryResponse> {
		const { student_id, quarter_id, form_lesson_id, subgroup_id } = request;

		return from(
			this.prisma.diary.findMany({
				where: {
					student: { id: student_id },
					timetable_entry: {
						form_lesson: {
							id: form_lesson_id,
							form_subgroup: { id: subgroup_id },
						},
						quarter_week: { quarter: { id: quarter_id } },
					},
				},
				include: {
					timetable_entry: {
						include: {
							quarter_week: true,
						},
					},
				},
				orderBy: [
					{ timetable_entry: { quarter_week: { start_date: "asc" } } },
					{ created_at: "asc" },
				],
			}),
		).pipe(
			map((r) => ({
				start_date: r
					.at(0)
					?.timetable_entry?.quarter_week?.start_date?.toISOString(),
				end_date: r
					.at(-1)
					?.timetable_entry?.quarter_week?.end_date?.toISOString(),
				diary: r,
			})),
		);
	}

	PostDiary(
		payload: TD.Types.IDiaryPayload,
	): Observable<TD.Types.IDiaryEntry[]> {
		return from(
			this.prisma.$transaction(
				payload.diary.map((v) =>
					this.prisma.diary.upsert({
						where: {
							unique: {
								student_id: v.student_id,
								timetable_entry_id: v.timetable_entry_id,
							},
						},
						create: {
							timetable_entry_id: v.timetable_entry_id,
							user_id: payload.user_id,
							student_id: v.student_id,
							comment: v.comment,
							marks: v.marks,
						},
						update: {
							marks: v.marks,
							comment: v.comment,
						},
					}),
				),
			),
		);
	}
}
