import { DiaryController } from "./diary.controller";
import { DiaryService } from "./diary.service";
import { Module } from "@nestjs/common";

@Module({
	controllers: [DiaryController],
	providers: [DiaryService],
	exports: [DiaryService],
})
export class DiaryModule {}
