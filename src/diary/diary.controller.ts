import { QueryToNumbersPipe } from "@td/pipes/query-to-numbers/query-to-numbers.pipe";
import { Body, Controller, Get, Post, Query } from "@nestjs/common";
import { DiaryService } from "./diary.service";
import { mergeMap } from "rxjs";
import { TD } from "@td";

@Controller("diary")
export class DiaryController {
	constructor(private readonly diaryService: DiaryService) {}

	@Get()
	GetDiary(
		@Query(QueryToNumbersPipe) request: TD.Types.IDiaryRequest,
	): TD.EndpointResponse<TD.Types.IDiaryResponse> {
		return this.diaryService
			.GetDiary(request)
			.pipe(mergeMap((d) => TD.CreateEndpointResponse(d)));
	}

	@TD.MinimalRole(TD.Types.UserRole.TEACHER)
	@Post()
	PostDiary(
		@Body() payload: TD.Types.IDiaryPayload,
	): TD.EndpointResponse<boolean> {
		return this.diaryService
			.PostDiary(payload)
			.pipe(mergeMap(() => TD.CreateEndpointResponse(true)));
	}
}
