import { FormsController } from "./forms.controller";
import { FormsService } from "./forms.service";
import { Module } from "@nestjs/common";

@Module({
	controllers: [FormsController],
	providers: [FormsService],
})
export class FormsModule {}
