import { FormsService } from "./forms.service";
import { mergeMap } from "rxjs";
import {
	Body,
	Controller,
	Delete,
	Get,
	Param,
	Patch,
	Post,
} from "@nestjs/common";
import { TD } from "@td";

@Controller("forms")
export class FormsController {
	constructor(private readonly formsService: FormsService) {}

	@Get()
	GetAll(): TD.EndpointResponse<TD.Types.IForm[]> {
		return this.formsService
			.GetAll()
			.pipe(mergeMap((f) => TD.CreateEndpointResponse(f)));
	}

	@Get("subgroups")
	GetSubgroups(): TD.EndpointResponse<TD.Types.IFormSubgroup[]> {
		return this.formsService
			.GetSubgroups()
			.pipe(mergeMap((s) => TD.CreateEndpointResponse(s)));
	}

	@Get("subgroups/:id")
	GetSubgroupById(
		@Param("id") id: number,
	): TD.EndpointResponse<TD.Types.IFormSubgroup> {
		return this.formsService
			.GetSubgroupById(id)
			.pipe(mergeMap((s) => TD.CreateEndpointResponse(s)));
	}

	@Get(":id")
	GetById(@Param("id") id: number): TD.EndpointResponse<TD.Types.IForm> {
		return this.formsService
			.GetById(id)
			.pipe(mergeMap((f) => TD.CreateEndpointResponse(f)));
	}

	@Get(":id/lessons")
	GetFormLessons(
		@Param("id") id: number,
	): TD.EndpointResponse<TD.Types.IFormLesson[]> {
		return this.formsService
			.GetFormLessons(id)
			.pipe(mergeMap((d) => TD.CreateEndpointResponse(d)));
	}

	@Get(":id/subgroups")
	GetFormSubgroups(
		@Param("id") form_id: number,
	): TD.EndpointResponse<TD.Types.IFormSubgroup[]> {
		return this.formsService
			.GetFormSubgroups(form_id)
			.pipe(mergeMap((d) => TD.CreateEndpointResponse(d)));
	}

	@TD.MinimalRole(TD.Types.UserRole.TEACHER)
	@Post()
	CreateOne(@Body() body: TD.Types.IForm): TD.EndpointResponse<TD.Types.IForm> {
		return this.formsService
			.CreateOne(body)
			.pipe(mergeMap((f) => TD.CreateEndpointResponse(f)));
	}

	@TD.MinimalRole(TD.Types.UserRole.TEACHER)
	@Post("subgroups")
	CreateSubgroup(
		@Body() body: TD.Types.IFormSubgroup,
	): TD.EndpointResponse<TD.Types.IFormSubgroup> {
		return this.formsService
			.CreateFormSubgroup(body)
			.pipe(mergeMap((s) => TD.CreateEndpointResponse(s)));
	}

	@TD.MinimalRole(TD.Types.UserRole.TEACHER)
	@Patch(":id")
	UpdateOne(
		@Param("id") id: number,
		@Body() payload: Partial<TD.Types.IForm>,
	): TD.EndpointResponse<TD.Types.IForm> {
		return this.formsService
			.UpdateOne(id, payload)
			.pipe(mergeMap((res) => TD.CreateEndpointResponse(res)));
	}

	@TD.MinimalRole(TD.Types.UserRole.TEACHER)
	@Patch("subgroups/:id")
	UpdateSubgroup(
		@Param("id") id: number,
		@Body() payload: Partial<TD.Types.IFormSubgroup>,
	): TD.EndpointResponse<TD.Types.IFormSubgroup> {
		return this.formsService
			.UpdateFormSubgroup(id, payload)
			.pipe(mergeMap((s) => TD.CreateEndpointResponse(s)));
	}

	@TD.MinimalRole(TD.Types.UserRole.TEACHER)
	@Delete(":id")
	DeleteOne(@Param("id") id: number): TD.EndpointResponse<TD.Types.IForm> {
		return this.formsService
			.DeleteOne(id)
			.pipe(mergeMap((res) => TD.CreateEndpointResponse(res)));
	}

	@TD.MinimalRole(TD.Types.UserRole.TEACHER)
	@Delete("subgroups/:id")
	DeleteSubgroup(
		@Param("id") id: number,
	): TD.EndpointResponse<TD.Types.IFormSubgroup> {
		return this.formsService
			.DeleteFormSubgroup(id)
			.pipe(mergeMap((res) => TD.CreateEndpointResponse(res)));
	}
}
