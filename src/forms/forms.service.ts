import { Injectable } from "@nestjs/common";

import { from, mergeMap, Observable, tap } from "rxjs";

import { PrismaService } from "@td-prisma";
import { Prisma } from "@prisma/client";

import { TD } from "@td";

@Injectable()
export class FormsService {
	private readonly _relations: Prisma.FormInclude = {
		students: {
			include: { user: true },
			orderBy: [
				{ user: { last_name: "asc" } },
				{ user: { first_name: "asc" } },
				{ user: { patronymic: "asc" } },
			],
		},
		master: { include: { user: true } },
		subgroups: true,
	};

	constructor(private readonly prisma: PrismaService) {}

	GetAll(): Observable<TD.Types.IForm[]> {
		return from(
			this.prisma.form.findMany({
				include: this._relations,
			}),
		);
	}

	GetById(id: number): Observable<TD.Types.IForm> {
		return from(
			this.prisma.form.findUnique({ where: { id }, include: this._relations }),
		).pipe(
			tap((v) => {
				if (!v)
					throw TD.CreateHttpException(
						TD.HTTP_STATUS.NOT_FOUND,
						"Form not found",
					);
			}),
		);
	}

	GetFormLessons(id: number): Observable<TD.Types.IFormLesson[]> {
		return from(
			this.prisma.form.findUnique({ where: { id } }).form_lessons({
				include: {
					lesson: true,
					form_subgroup: { include: { students: { include: { user: true } } } },
					teacher: { include: { user: true } },
				},
			}),
		);
	}

	GetSubgroups(): Observable<TD.Types.IFormSubgroup[]> {
		return from(
			this.prisma.formSubgroup.findMany({
				include: { students: { include: { user: true } } },
			}),
		);
	}

	GetSubgroupById(id: number): Observable<TD.Types.IFormSubgroup> {
		return from(
			this.prisma.formSubgroup.findUnique({
				where: { id },
				include: {
					form: { include: { students: { include: { user: true } } } },
					students: { include: { user: true } },
				},
			}),
		).pipe(
			tap((v) => {
				if (!v)
					throw TD.CreateHttpException(
						TD.HTTP_STATUS.NOT_FOUND,
						"Subgroup not found",
					);
			}),
		);
	}

	GetFormSubgroups(form_id: number): Observable<TD.Types.IFormSubgroup[]> {
		return from(
			this.prisma.form
				.findUnique({ where: { id: form_id } })
				.subgroups({ include: { students: { include: { user: true } } } }),
		);
	}

	CreateOne(
		payload: TD.Types.DeepPartial<TD.Types.IForm>,
	): Observable<TD.Types.IForm> {
		return from(
			this.prisma.form.create({
				data: {
					grade: payload.grade,
					letter: payload.letter,
					master_id: payload.master?.id,
					students: { connect: payload.students?.map((v) => ({ id: v.id })) },
				},
			}),
		);
	}

	CreateFormSubgroup(
		payload: TD.Types.DeepPartial<TD.Types.IFormSubgroup>,
	): Observable<TD.Types.IFormSubgroup> {
		return this.GetById(<any>payload.form).pipe(
			mergeMap((f) => {
				if (!this._ValidateStudents(f, payload.students))
					throw TD.CreateHttpException(
						TD.HTTP_STATUS.BAD_REQUEST,
						"Students do not belong to the same form",
					);

				return this.prisma.formSubgroup.create({
					data: {
						name: payload.name,
						form_id: payload.form?.id,
						students: { connect: payload.students?.map((v) => ({ id: v.id })) },
					},
				});
			}),
		);
	}

	UpdateOne(
		id: number,
		payload: TD.Types.DeepPartial<TD.Types.IForm>,
	): Observable<TD.Types.IForm> {
		return from(
			this.prisma.form.update({
				where: { id },
				data: {
					grade: payload.grade,
					letter: payload.letter,
					graduated: payload.graduated,
					master_id: payload.master?.id,
					students: { set: payload.students?.map((v) => ({ id: v.id })) },
				},
			}),
		);
	}

	UpdateFormSubgroup(
		id: number,
		payload: TD.Types.DeepPartial<TD.Types.IFormSubgroup>,
	): Observable<TD.Types.IFormSubgroup> {
		return this.GetSubgroupById(id).pipe(
			mergeMap((s) => {
				if (!this._ValidateStudents(s.form, payload.students))
					throw TD.CreateHttpException(
						TD.HTTP_STATUS.BAD_REQUEST,
						"Students do not belong to the same form",
					);

				return from(
					this.prisma.formSubgroup.update({
						where: { id },
						data: {
							name: payload.name,
							form_id: payload.form?.id,
							students: { set: payload.students?.map((v) => ({ id: v.id })) },
						},
					}),
				);
			}),
		);
	}

	DeleteOne(id: number): Observable<TD.Types.IForm> {
		return from(this.prisma.form.delete({ where: { id } }));
	}

	DeleteFormSubgroup(id: number): Observable<TD.Types.IFormSubgroup> {
		return from(this.prisma.formSubgroup.delete({ where: { id } }));
	}

	private _ValidateStudents(
		form: TD.Types.IForm,
		students?: { id?: number }[],
	): boolean {
		const form_students = form.students.map((s) => s.id);
		return students?.every((student) => form_students.includes(student.id));
	}
}
