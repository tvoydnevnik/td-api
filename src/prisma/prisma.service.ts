import { Injectable, OnModuleInit } from "@nestjs/common";

import { PrismaClient } from "@prisma/client";
import {
	PrismaCurrentPeriodsOfTimeExtension,
	PrismaPeriodsOfTimeExtensions,
} from "@prisma-extensions";

@Injectable()
export class PrismaService extends PrismaClient implements OnModuleInit {
	async onModuleInit() {
		await this.$connect();
	}

	// Keep the chaining clean here without formatting
	// prettier-ignore

	/**
	 * Default Prisma client with applied extensions
	 */
	public readonly extended = this
		.$extends(PrismaCurrentPeriodsOfTimeExtension)
		.$extends(PrismaPeriodsOfTimeExtensions);
}
