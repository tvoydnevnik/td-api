import { Prisma } from "@prisma/client";
import { TD } from "@td";

export const findMaxDate =
	<T extends Record<string, any>>(comparisonField: keyof T) =>
	(max: T, curr: T) =>
		new Date(curr[comparisonField]) > new Date(max[comparisonField])
			? curr
			: max;

export const PrismaCurrentPeriodsOfTimeExtension = Prisma.defineExtension((c) =>
	c.$extends({
		name: "PrismaCurrentPeriodsOfTimeExtension",
		model: {
			quarterWeek: {
				findCurrent: async (
					opts?: Prisma.QuarterWeekFindManyArgs,
				): Promise<
					Prisma.QuarterWeekGetPayload<Prisma.QuarterWeekDefaultArgs>
				> => {
					const now = new Date();
					const r = await c.quarterWeek.findMany({
						...opts,
						where: {
							...opts?.where,
							OR: [
								{ indexing_start: { lte: now }, indexing_end: { gte: now } },
								{
									indexing_start: { not: { lte: now } },
									indexing_end: { not: { gte: now } },
									quarter: { school_year: { active: true } },
								},
							],
						},
					});

					if (r.length === 0)
						throw TD.CreateHttpException(
							TD.HTTP_STATUS.INTERNAL_SERVER_ERROR,
							"Failed to determine current quarter week",
						);

					return r?.reduce(findMaxDate("indexing_end"));
				},
			},
			quarter: {
				findCurrent: async (
					opts?: Prisma.QuarterFindManyArgs,
				): Promise<Prisma.QuarterGetPayload<Prisma.QuarterDefaultArgs>> => {
					const now = new Date();
					const r = await c.quarter.findMany({
						...opts,
						where: {
							...opts?.where,
							OR: [
								{ indexing_start: { lte: now }, indexing_end: { gte: now } },
								{
									indexing_start: { not: { lte: now } },
									indexing_end: { not: { gte: now } },
									school_year: { active: true },
								},
							],
						},
					});

					if (r.length === 0)
						throw TD.CreateHttpException(
							TD.HTTP_STATUS.INTERNAL_SERVER_ERROR,
							"Failed to determine current quarter",
						);

					return r?.reduce(findMaxDate("indexing_end"));
				},
			},
			schoolYear: {
				findCurrent: async (
					opts?: Prisma.SchoolYearFindUniqueArgs,
				): Promise<
					Prisma.SchoolYearGetPayload<Prisma.SchoolYearDefaultArgs>
				> => {
					return await c.schoolYear
						.findUnique({
							...opts,
							where: {
								...opts?.where,
								active: true,
							},
						})
						.then((v) => {
							if (!v)
								throw TD.CreateHttpException(
									TD.HTTP_STATUS.INTERNAL_SERVER_ERROR,
									"Failed to determine current school year",
								);

							return v;
						});
				},
			},
		},
	}),
);
