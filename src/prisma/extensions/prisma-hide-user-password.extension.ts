import { Prisma } from "@prisma/client";

export const PrismaHideUserPasswordExtension = Prisma.defineExtension((c) =>
	c.$extends({
		name: "PrismaHideUserPasswordExtension",
		query: {
			user: {
				async $allOperations({ operation, query, args }) {
					switch (operation) {
						case "aggregate":
						case "count":
						case "createMany":
						case "deleteMany":
						case "updateMany":
						case "groupBy":
							return query(args);
						default:
							const res = (await query(args)) as
								| Record<string, any>
								| Array<Record<string, any>>;
							if (args?.select?.password !== true) {
								Array.isArray(res)
									? res?.map((user: Record<string, any>) => {
											delete user.password;
											return user;
									  })
									: delete res.password;
							}

							return res;
					}
				},
			},
		},
	}),
);
