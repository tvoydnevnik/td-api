import { updateWithQuarters } from "./school-year/school-year-update-with-quarters";
import { createWithActive } from "./school-year/school-year-create-with-active";
import { updateWithActive } from "./school-year/school-year-update-with-active";
import { updateWithWeeks } from "./quarter/quarter-update-with-weeks";
import { supersede } from "./school-year/school-year-supersede";

import { Prisma } from "@prisma/client";

export type UpdateActiveAction = "update" | "supersede";

export const PrismaPeriodsOfTimeExtensions = Prisma.defineExtension((c) =>
	c.$extends({
		name: "PrismaPeriodsOfTimeExtensions",
		model: {
			schoolYear: {
				supersede: (
					new_school_year_id: number,
					args?: Prisma.SchoolYearDefaultArgs,
				) =>
					supersede(c as Prisma.DefaultPrismaClient, new_school_year_id, args),
				createWithActive: (
					args: Prisma.SchoolYearCreateArgs,
					update_active_action: UpdateActiveAction = "supersede",
				) =>
					createWithActive(
						c as Prisma.DefaultPrismaClient,
						args,
						update_active_action,
					),
				updateWithActive: (
					args: Prisma.SchoolYearUpdateArgs,
					update_active_action: UpdateActiveAction = "update",
				) =>
					updateWithActive(
						c as Prisma.DefaultPrismaClient,
						args,
						update_active_action,
					),
				updateWithQuarters: (args: Prisma.SchoolYearUpdateArgs) =>
					updateWithQuarters(c as Prisma.DefaultPrismaClient, args),
			},
			quarter: {
				updateWithWeeks: (args: Prisma.QuarterUpdateArgs) =>
					updateWithWeeks(c as Prisma.DefaultPrismaClient, args),
			},
		},
	}),
);
