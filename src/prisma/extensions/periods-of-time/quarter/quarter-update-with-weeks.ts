import { Prisma } from "@prisma/client";

import { FillQuarterWeeks } from "../../../../quarters/fill-quarter-weeks";

import { isDefaultPrismaClient } from "@td/type-guards/is-default-prisma-client";

// TODO: Check if lessons that are bound to quarter's weeks should be included or deleted based on their weekday value.
export async function updateWithWeeks(
	client: Prisma.DefaultPrismaClient | Prisma.TransactionClient,
	args: Prisma.QuarterUpdateArgs,
): Promise<Prisma.QuarterGetPayload<typeof args>> {
	const ops = async (
		client: Prisma.DefaultPrismaClient | Prisma.TransactionClient,
	) => {
		const q = await client.quarter.update(args);
		const qws = await FillQuarterWeeks(client, q);
		await client.quarterWeek.deleteMany({
			where: { quarter_id: q.id, index: { gt: qws.last_index } },
		});

		return q;
	};

	return isDefaultPrismaClient(client)
		? client.$transaction(async (tx) => ops(tx))
		: ops(client);
}
