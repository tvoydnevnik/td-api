import { updateActive } from "./school-year-update-active";

import { isDefaultPrismaClient } from "@td/type-guards/is-default-prisma-client";

import { Prisma } from "@prisma/client";

export async function supersede(
	client: Prisma.DefaultPrismaClient | Prisma.TransactionClient,
	new_school_year_id: number,
	args?: Prisma.SchoolYearDefaultArgs,
): Promise<Prisma.SchoolYearGetPayload<typeof args>> {
	const ops = async (
		client: Prisma.DefaultPrismaClient | Prisma.TransactionClient,
	) => {
		await client.form.updateMany({
			where: { graduated: false },
			data: { grade: { increment: 1 } },
		});
		return updateActive(client, new_school_year_id, args);
	};

	return isDefaultPrismaClient(client)
		? client.$transaction(async (tx) => ops(tx))
		: ops(client);
}
