import { updateWithWeeks } from "../quarter/quarter-update-with-weeks";
import { updateWithActive } from "./school-year-update-with-active";

import { Prisma } from "@prisma/client";

import { isDefaultPrismaClient } from "@td/type-guards/is-default-prisma-client";

export async function updateWithQuarters(
	client: Prisma.DefaultPrismaClient | Prisma.TransactionClient,
	args: Prisma.SchoolYearUpdateArgs,
): Promise<Prisma.SchoolYearGetPayload<typeof args>> {
	const ops = async (
		client: Prisma.DefaultPrismaClient | Prisma.TransactionClient,
	) => {
		const y = (await updateWithActive(
			client,
			{
				...args,
				include: {
					...args.include,
					quarters: { orderBy: { start_date: "asc" } },
				},
			},
			"update",
		)) as Prisma.SchoolYearGetPayload<{ include: { quarters: true } }>;

		// Double-check so we don't waste processing power
		// on updating more than we need to.
		if (!args.data.start_date && !args.data.end_date) return y;
		if (y.quarters.length === 0) return y;
		if (y.quarters.length === 1) {
			await updateWithWeeks(client, {
				where: {
					id: y.quarters.at(0).id,
				},
				data: {
					start_date: y.start_date,
					indexing_start: y.start_date,
					end_date: y.end_date,
					indexing_end: y.end_date,
				},
			});

			return y;
		}

		const first_q = y.quarters.at(0);
		await updateWithWeeks(client, {
			where: { id: first_q.id },
			data: {
				start_date: y.start_date,
				indexing_start: y.start_date,
			},
		});

		const last_q = y.quarters.at(-1);
		await updateWithWeeks(client, {
			where: { id: last_q.id },
			data: { end_date: y.end_date, indexing_end: y.end_date },
		});

		return y;
	};

	return isDefaultPrismaClient(client)
		? client.$transaction(async (tx) => ops(tx))
		: ops(client);
}
