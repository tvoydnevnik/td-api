import { updateActive } from "./school-year-update-active";
import { supersede } from "./school-year-supersede";

import { UpdateActiveAction } from "@prisma-extensions";
import { Prisma } from "@prisma/client";

import { isDefaultPrismaClient } from "@td/type-guards/is-default-prisma-client";

export async function createWithActive(
	client: Prisma.DefaultPrismaClient | Prisma.TransactionClient,
	args: Prisma.SchoolYearCreateArgs,
	update_active_action: UpdateActiveAction = "supersede",
): Promise<Prisma.SchoolYearGetPayload<typeof args>> {
	const ops = async (
		client: Prisma.DefaultPrismaClient | Prisma.TransactionClient,
	) => {
		const year = await client.schoolYear.create({
			...args,
			data: { ...args.data, active: undefined },
		});

		if (args.data.active)
			return update_active_action === "supersede"
				? supersede(client, year.id, args)
				: updateActive(client, year.id, args);

		return year;
	};

	return isDefaultPrismaClient(client)
		? client.$transaction(async (tx) => ops(tx))
		: ops(client);
}
