import { Prisma } from "@prisma/client";

import { isDefaultPrismaClient } from "@td/type-guards/is-default-prisma-client";

export async function updateActive(
	client: Prisma.DefaultPrismaClient | Prisma.TransactionClient,
	id: number,
	args?: Prisma.SchoolYearDefaultArgs,
): Promise<Prisma.SchoolYearGetPayload<typeof args>> {
	const ops = async (
		client: Prisma.DefaultPrismaClient | Prisma.TransactionClient,
	) => {
		await client.schoolYear.updateMany({
			where: { active: true },
			data: { active: null },
		});

		return client.schoolYear.update({
			where: { id: id },
			data: { active: true },
			...args,
		});
	};

	return isDefaultPrismaClient(client)
		? client.$transaction(async (tx) => ops(tx))
		: ops(client);
}
