import { Global, Module } from "@nestjs/common";

import { PrismaHideUserPasswordExtension } from "@prisma-extensions";
import { PrismaService } from "@td-prisma";

@Global()
@Module({
	providers: [
		// prettier-ignore
		{
			provide: PrismaService,
			useValue: new PrismaService()
				.$extends(PrismaHideUserPasswordExtension),
		},
	],
	exports: [PrismaService],
})
export class PrismaModule {}
