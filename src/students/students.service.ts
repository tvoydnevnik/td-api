import { Injectable } from "@nestjs/common";

import { from, Observable, tap } from "rxjs";

import { PrismaService } from "@td-prisma";

import { TD } from "@td";

@Injectable()
export class StudentsService {
	constructor(private readonly prisma: PrismaService) {}

	GetAll(): Observable<TD.Types.IStudent[]> {
		return from(
			this.prisma.student.findMany({ include: { user: true } }),
		) as Observable<TD.Types.IStudent[]>;
	}

	GetById(id: number): Observable<TD.Types.IStudent> {
		return from(
			this.prisma.student.findUnique({
				where: { id },
				include: {
					user: true,
					form: { include: { master: { include: { user: true } } } },
					parents: { include: { user: true } },
				},
			}),
		).pipe(
			tap((v) => {
				if (!v)
					throw TD.CreateHttpException(
						TD.HTTP_STATUS.NOT_FOUND,
						"Student not found",
					);
			}),
		) as Observable<TD.Types.IStudent>;
	}

	UpdateOne(
		id: number,
		payload: TD.Types.DeepPartial<TD.Types.IStudent>,
	): Observable<TD.Types.IStudent> {
		return from(
			this.prisma.student.update({
				where: { id },
				data: {
					form_id: payload.form?.id,
					form_subgroups: {
						set: payload.form_subgroups?.map((v) => ({ id: v.id })),
					},
					parents: {
						set: payload.parents?.map((v) => ({ id: v.id })),
					},
				},
			}),
		);
	}

	DeleteOne(id: number): Observable<TD.Types.IStudent> {
		return from(this.prisma.student.delete({ where: { id } }));
	}
}
