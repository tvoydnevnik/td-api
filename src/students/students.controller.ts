import { Body, Controller, Delete, Get, Param, Patch } from "@nestjs/common";
import { StudentsService } from "./students.service";
import { mergeMap } from "rxjs";
import { TD } from "@td";

@Controller("students")
export class StudentsController {
	constructor(private readonly studentsService: StudentsService) {}

	@Get()
	GetAll(): TD.EndpointResponse<TD.Types.IStudent[]> {
		return this.studentsService
			.GetAll()
			.pipe(mergeMap((parents) => TD.CreateEndpointResponse(parents)));
	}

	@Get(":id")
	GetById(@Param("id") id: number): TD.EndpointResponse<TD.Types.IStudent> {
		return this.studentsService
			.GetById(id)
			.pipe(mergeMap((parent) => TD.CreateEndpointResponse(parent)));
	}

	@TD.MinimalRole(TD.Types.UserRole.TEACHER)
	@Patch(":id")
	UpdateOne(
		@Param("id") id: number,
		@Body() body: Partial<TD.Types.IStudent>,
	): TD.EndpointResponse<TD.Types.IStudent> {
		return this.studentsService
			.UpdateOne(id, body)
			.pipe(mergeMap((parent) => TD.CreateEndpointResponse(parent)));
	}

	@TD.MinimalRole(TD.Types.UserRole.TEACHER)
	@Delete(":id")
	DeleteOne(@Param("id") id: number): TD.EndpointResponse<TD.Types.IStudent> {
		return this.studentsService
			.DeleteOne(id)
			.pipe(mergeMap((res) => TD.CreateEndpointResponse(res)));
	}
}
