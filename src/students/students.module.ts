import { StudentsController } from "./students.controller";
import { StudentsService } from "./students.service";
import { Module } from "@nestjs/common";

@Module({
	controllers: [StudentsController],
	providers: [StudentsService],
})
export class StudentsModule {}
