import { Injectable } from "@nestjs/common";

import { from, iif, map, mergeMap, Observable, of, tap } from "rxjs";

import { PrismaService } from "@td-prisma";
import { Prisma } from "@prisma/client";

import { hash, verify } from "argon2";

import { TD } from "@td";

@Injectable()
export class UsersService {
	constructor(private readonly prisma: PrismaService) {}

	GetAll(): Observable<TD.Types.IUser[]> {
		return from(
			this.prisma.user.findMany({
				orderBy: { id: "asc" },
				include: { invitations: true },
			}),
		) as Observable<TD.Types.IUser[]>;
	}

	GetById(id: number): Observable<TD.Types.IUser> {
		return from(this.prisma.user.findUnique({ where: { id } })).pipe(
			tap((v) => {
				if (!v)
					throw TD.CreateHttpException(
						TD.HTTP_STATUS.NOT_FOUND,
						"User not found",
					);
			}),
		) as Observable<TD.Types.IUser>;
	}

	GetByIdentifierWithPassword(identifier: string): Observable<
		TD.IRequestUser & {
			password?: string;
		}
	> {
		return from(
			this.prisma.user.findFirst({
				where: { OR: [{ username: identifier }, { email: identifier }] },
				select: {
					// On the frontend we retrieve user profile by sending GET /users/me
					// Which uses stored user.id to get a full profile from the db
					// So we only need password and id for thing to work
					// We also need role for RolesGuard to work
					id: true,
					role: true,
					password: true,
				},
			}),
		).pipe(
			tap((v) => {
				if (!v)
					throw TD.CreateHttpException(
						TD.HTTP_STATUS.NOT_FOUND,
						"User not found",
					);
			}),
		);
	}

	GetUnregistered(): Observable<TD.Types.IUser[]> {
		return from(
			this.prisma.user.findMany({ where: { is_registered: false } }),
		) as Observable<TD.Types.IUser[]>;
	}

	UpdateOne(
		contextUser: TD.IRequestUser,
		id: number,
		payload: TD.Types.DeepPartial<TD.Types.IUser> & {
			password?: string;
		},
		unsafe = false,
	): Observable<TD.Types.IUser> {
		const data: Prisma.UserUpdateInput = unsafe
			? payload
			: TD.SafeUpdate(
					TD.BuildConstraints({
						constraints: TD.ObjectPropGuards.User,
						user: contextUser,
					}),
					{},
					payload,
			  );

		return iif(
			() => !!payload.password,
			hash(payload.password),
			of(undefined),
		).pipe(
			mergeMap((hashedPassword) =>
				from(this.prisma.user.findUnique({ where: { id } })).pipe(
					map((user) => ({
						hashedPassword,
						user,
					})),
				),
			),
			mergeMap(({ hashedPassword, user }) => {
				const role_data: Prisma.UserUpdateInput = {};

				if (payload.role && payload.role !== user.role) {
					role_data[TD.Types.GetRoleString(user.role)] = { delete: true };
					role_data[TD.Types.GetRoleString(payload.role)] = { create: {} };
				}

				return this.prisma.user.update({
					where: { id },
					data: {
						...data,
						password: hashedPassword,
						...role_data,
					},
				});
			}),
		) as Observable<TD.Types.IUser>;
	}

	DeleteOne(id: number): Observable<TD.Types.IUser> {
		return from(
			this.prisma.user.delete({ where: { id } }),
		) as Observable<TD.Types.IUser>;
	}

	VerifyPassword(
		identifier: string,
		password: string,
	): Observable<TD.IRequestUser | null> {
		return this.GetByIdentifierWithPassword(identifier).pipe(
			mergeMap((user) => {
				return iif(
					() => !!user?.password,
					verify(user.password, password),
					of(false),
				).pipe(map((match) => ({ match, user })));
			}),
			map(({ match, user }) => {
				if (!match) return null;

				delete user.password;
				return user;
			}),
		);
	}

	CompleteRegistration(user: TD.Types.IUser): Observable<TD.Types.IUser> {
		return from(
			this.prisma.user.update({
				where: { id: user.id },
				data: {
					invitations: { deleteMany: {} },
					is_registered: true,
				},
			}),
		) as Observable<TD.Types.IUser>;
	}
}
