import { Test, TestingModule } from "@nestjs/testing";
import { UsersService } from "./users.service";
import { firstValueFrom } from "rxjs";
import { TD } from "@td";

describe("UserService", () => {
	let service: UsersService;
	// let dataSource: DataSource;

	beforeEach(async () => {
		// dataSource = await TD.Testing.CreateDataSource();
		// await TD.Testing.MockUsers.Init(dataSource);

		const module: TestingModule = await Test.createTestingModule({
			// providers: [
			// 	UsersService,
			// 	TD.Testing.CreateCustomProvider(
			// 		TD.UserEntity,
			// 		dataSource.getRepository(TD.UserEntity),
			// 	),
			// ],
		}).compile();

		service = module.get<UsersService>(UsersService);
	});

	it("should be defined", () => {
		expect(service).toBeDefined();
	});

	it("should get by identifier", async function () {
		// const byUsername = await firstValueFrom(
		// 	service.GetByIdentifier(TD.Testing.MockUsers.Reference.username),
		// );
		// const byEmail = await firstValueFrom(
		// 	service.GetByIdentifier(TD.Testing.MockUsers.Reference.email),
		// );
		//
		// const badIdentifier = await firstValueFrom(
		// 	service.GetByIdentifier(
		// 		TD.Testing.MockUsers.Reference.badIdentifier,
		// 	),
		// );
		// expect(byUsername.username).toBe(
		// 	TD.Testing.MockUsers.Reference.username,
		// );
		// expect(byEmail.email).toBe(TD.Testing.MockUsers.Reference.email);
		// expect(badIdentifier).toBeNull();
	});

	it("should create one", async function () {
		// const createOne = await firstValueFrom(
		// 	service.CreateOne({
		// 		username: TD.Testing.MockUsers.Reference.anotherUsername,
		// 		email: TD.Testing.MockUsers.Reference.anotherEmail,
		// 		password: TD.Testing.MockUsers.Reference.goodPassword,
		// 	}),
		// );
		//
		// expect(createOne).toBeDefined();
		//
		// expect(createOne.id).toBeDefined();
		// expect(createOne.username).toBe(
		// 	TD.Testing.MockUsers.Reference.anotherUsername,
		// );
		// expect(createOne.email).toBe(
		// 	TD.Testing.MockUsers.Reference.anotherEmail,
		// );
	});

	it("should verify password", async function () {
		// const goodPassword = await firstValueFrom(
		// 	service.VerifyPassword(
		// 		TD.Testing.MockUsers.Reference.username,
		// 		TD.Testing.MockUsers.Reference.goodPassword,
		// 	),
		// );
		//
		// const badPassword = await firstValueFrom(
		// 	service.VerifyPassword(
		// 		TD.Testing.MockUsers.Reference.username,
		// 		TD.Testing.MockUsers.Reference.badPassword,
		// 	),
		// );
		//
		// expect(goodPassword).toBeTruthy();
		// expect(badPassword).toBeFalsy();
	});

	// TODO: Provide unit testing for user updates
});
