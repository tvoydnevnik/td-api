import {
	Body,
	Controller,
	Delete,
	Get,
	Param,
	Patch,
	Request,
	UseGuards,
} from "@nestjs/common";

import { mergeMap } from "rxjs";

import { UsersService } from "./users.service";

import { TD } from "@td";

@Controller("users")
export class UsersController {
	constructor(private readonly usersService: UsersService) {}

	@TD.MinimalRole(TD.Types.UserRole.ADMIN)
	@Get()
	GetAll(): TD.EndpointResponse<TD.Types.IUser[]> {
		return this.usersService
			.GetAll()
			.pipe(mergeMap((u) => TD.CreateEndpointResponse(u)));
	}

	@TD.MinimalRole(TD.Types.UserRole.TEACHER)
	@Get("unregistered")
	GetUnregistered(): TD.EndpointResponse<TD.Types.IUser[]> {
		return this.usersService
			.GetUnregistered()
			.pipe(mergeMap((u) => TD.CreateEndpointResponse(u)));
	}

	@UseGuards(TD.AuthenticatedGuard)
	@Get("me")
	GetMe(@Request() req: TD.IRequest): TD.EndpointResponse<TD.Types.IUser> {
		return this.usersService
			.GetById(req.user.id)
			.pipe(mergeMap((user) => TD.CreateEndpointResponse(user)));
	}

	@UseGuards(TD.AuthenticatedGuard)
	@Patch("me")
	UpdateMe(
		@Request() req: TD.IRequest,
		@Body() payload: TD.Types.IUser,
	): TD.EndpointResponse<TD.Types.IUser> {
		return this.usersService
			.UpdateOne(req.user, req.user.id, payload)
			.pipe(mergeMap((user) => TD.CreateEndpointResponse(user)));
	}

	@Get(":id")
	GetById(@Param("id") id: number): TD.EndpointResponse<TD.Types.IUser> {
		return this.usersService
			.GetById(id)
			.pipe(mergeMap((user) => TD.CreateEndpointResponse(user)));
	}

	@TD.MinimalRole(TD.Types.UserRole.ADMIN)
	@Patch(":id")
	UpdateById(
		@Request() req: TD.IRequest,
		@Param("id") id: number,
		@Body() body: Partial<TD.Types.IUser>,
	): TD.EndpointResponse<TD.Types.IUser> {
		return this.usersService
			.UpdateOne(req.user, id, body)
			.pipe(mergeMap((user) => TD.CreateEndpointResponse(user)));
	}

	@TD.MinimalRole(TD.Types.UserRole.ADMIN)
	@Delete(":id")
	DeleteOne(@Param("id") id: number): TD.EndpointResponse<TD.Types.IUser> {
		return this.usersService
			.DeleteOne(id)
			.pipe(mergeMap((res) => TD.CreateEndpointResponse(res)));
	}
}
