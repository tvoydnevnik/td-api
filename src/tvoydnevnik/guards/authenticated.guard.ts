import { CanActivate, ExecutionContext, Injectable } from "@nestjs/common";
import { TD } from "@td";

@Injectable()
export class AuthenticatedGuard implements CanActivate {
	canActivate(context: ExecutionContext): boolean {
		const req = context.switchToHttp().getRequest();
		if (!req.isAuthenticated())
			throw TD.CreateHttpException(TD.HTTP_STATUS.UNAUTHORIZED, "Unauthorized");

		return true;
	}
}
