import { ExecutionContext, Injectable } from "@nestjs/common";
import { from, Observable, of, switchMap } from "rxjs";
import { AuthGuard } from "@nestjs/passport";

@Injectable()
export class LoginGuard extends AuthGuard("local") {
	constructor() {
		super();
	}

	canActivate(context: ExecutionContext): Observable<boolean> {
		let canActivateRes = false;

		return from(super.canActivate(context) as Promise<boolean>).pipe(
			switchMap((res) => {
				canActivateRes = res;

				const req = context.switchToHttp().getRequest();
				return from(super.logIn(req));
			}),
			switchMap(() => {
				return of(canActivateRes);
			}),
		);
	}
}
