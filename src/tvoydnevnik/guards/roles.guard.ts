import { Reflector } from "@nestjs/core";
import { CanActivate, ExecutionContext, Injectable } from "@nestjs/common";

import { ROLES_KEY } from "../decorators/roles.decorator";

import { IRequestUser } from "@td/interfaces/request-user.interface";
import { Types } from "td-types";
import { TD } from "@td";

@Injectable()
export class RolesGuard implements CanActivate {
	constructor(private readonly reflector: Reflector) {}

	canActivate(context: ExecutionContext): boolean {
		const minimalRole: Types.UserRole = this.reflector.getAllAndOverride(
			ROLES_KEY,
			[context.getHandler(), context.getClass()],
		);

		if (!minimalRole) return true;

		const user: IRequestUser = context.switchToHttp().getRequest().user;
		if (!user)
			throw TD.CreateHttpException(TD.HTTP_STATUS.UNAUTHORIZED, "Unauthorized");

		const roleSufficient = user.role >= minimalRole;

		if (!roleSufficient)
			throw TD.CreateHttpException(
				TD.HTTP_STATUS.FORBIDDEN,
				"Insufficient privilege",
			);

		return true;
	}
}
