import { Injectable } from "@nestjs/common";

import { PassportStrategy } from "@nestjs/passport";
import { Strategy } from "passport-local";

import { UsersService } from "../../users/users.service";

import { firstValueFrom } from "rxjs";

import { TD } from "@td";

@Injectable()
export class LocalPassportStrategy extends PassportStrategy(Strategy) {
	constructor(private readonly usersService: UsersService) {
		super({
			usernameField: "identifier",
		});
	}

	async validate(
		identifier: string,
		password: string,
	): Promise<TD.IRequestUser> {
		const user = await firstValueFrom(
			this.usersService.VerifyPassword(identifier, password),
		);

		if (!user)
			throw TD.CreateHttpException(
				TD.HTTP_STATUS.UNAUTHORIZED,
				"Invalid user credentials",
			);

		return user;
	}
}
