import { createParamDecorator, ExecutionContext } from "@nestjs/common";
import { fromZodError } from "zod-validation-error";
import { TD } from "@td";
import { z } from "zod";

export interface ZodValidateConfig<T extends z.ZodTypeAny> {
	throw: boolean;
	validator: T;
}

export const ZodValidateBody = createParamDecorator(
	<T extends z.ZodTypeAny>(
		data: ZodValidateConfig<T> | undefined = undefined,
		ctx: ExecutionContext,
	): T | null => {
		const body = ctx.switchToHttp().getRequest().body;

		if (!data) return null;
		if (!data.validator) return null;

		const res = data.validator.safeParse(body);
		if (res.success === true) return res.data;
		if (data.throw) {
			const e = fromZodError(res.error);
			throw TD.CreateHttpException(TD.HTTP_STATUS.BAD_REQUEST, e.message, e);
		}
		return null;
	},
);
