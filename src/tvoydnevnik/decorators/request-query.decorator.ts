import {
	createParamDecorator,
	ExecutionContext,
	HttpException,
} from "@nestjs/common";

import { fromZodError } from "zod-validation-error";
import { z } from "zod";

import { TD } from "@td";

export class RequestQueryPipeConfig {
	public readonly validator?: z.ZodObject<any>;
	public readonly throw: boolean = true;
	public readonly allowEmptyRequest?: boolean = false;
}

export const RequestQuery_fac = (
	data: RequestQueryPipeConfig = { throw: true },
	ctx: ExecutionContext,
): object | null => {
	const req = ctx.switchToHttp().getRequest();
	const request_string = req.query["request"] as string;

	try {
		const parsed = JSON.parse(request_string);

		if (!data.allowEmptyRequest && Object.keys(parsed).length === 0) {
			throw TD.CreateHttpException(
				TD.HTTP_STATUS.BAD_REQUEST,
				"Empty requests are prohibited for this endpoint",
			);
		}

		if (!data.validator) {
			return parsed;
		}

		return data.validator.parse(parsed);
	} catch (e) {
		if (!data.throw) {
			return null;
		}

		if (e instanceof HttpException && !data.allowEmptyRequest) {
			throw e;
		}

		throw TD.CreateHttpException(
			TD.HTTP_STATUS.BAD_REQUEST,
			"Invalid payload",
			{
				details:
					e instanceof z.ZodError
						? fromZodError(e).message
						: "message" in e
						? e.message
						: e,
			},
		);
	}
};

export const RequestQuery = createParamDecorator(RequestQuery_fac);
