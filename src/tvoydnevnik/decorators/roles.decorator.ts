import { applyDecorators, SetMetadata, UseGuards } from "@nestjs/common";
import { AuthenticatedGuard } from "../guards/authenticated.guard";
import { TD } from "@td";

export const ROLES_KEY = "roles";
export const MinimalRole = (role: TD.Types.UserRole) => {
	return applyDecorators(
		UseGuards(AuthenticatedGuard),
		SetMetadata(ROLES_KEY, role),
	);
};
