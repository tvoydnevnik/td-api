import { Prisma } from "@prisma/client";

export function isDefaultPrismaClient(
	client: Prisma.DefaultPrismaClient | Prisma.TransactionClient,
): client is Prisma.DefaultPrismaClient {
	return "$transaction" in client;
}
