import { QueryToNumbersPipe } from "./query-to-numbers.pipe";

describe("QueryToNumbersPipe", () => {
	let pipe: QueryToNumbersPipe;

	beforeEach(() => {
		pipe = new QueryToNumbersPipe();
	});

	it("should be defined", () => {
		expect(pipe).toBeDefined();
	});

	it("should transform all string fields of query object to numbers", function () {
		const test_query = {
			id: "34",
			age: "18",
		};

		const r = pipe.transform(test_query);
		expect(r["id"]).toEqual(34);
		expect(typeof r["id"]).toBe("number");
		expect(r["age"]).toEqual(18);
		expect(typeof r["age"]).toBe("number");
	});
});
