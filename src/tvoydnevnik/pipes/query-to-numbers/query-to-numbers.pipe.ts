import { Injectable, PipeTransform } from "@nestjs/common";

@Injectable()
export class QueryToNumbersPipe implements PipeTransform {
	transform(value: object): object {
		const tmp = value;

		Object.keys(value).forEach((v) => {
			tmp[v] = Number(value[v]);
		});

		return tmp;
	}
}
