import { catchError, Observable } from "rxjs";
import {
	CallHandler,
	ExecutionContext,
	Injectable,
	NestInterceptor,
} from "@nestjs/common";
import { TD } from "@td";

const is_production = process.env["NODE_ENVIRONMENT"] === "production";

@Injectable()
export class ExceptionInterceptor implements NestInterceptor {
	intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
		return next.handle().pipe(
			catchError((err: any) => {
				// leave TD exceptions untouched
				// we also don't need to log them as they are usually not runtime errors
				if (err.response instanceof TD.CommonResponse) throw err;

				if (!is_production)
					console.error(">> intercepted exception:", err);

				// pass string errors as is
				if (typeof err === "string")
					throw TD.CreateHttpException(
						TD.HTTP_STATUS.INTERNAL_SERVER_ERROR,
						err,
					);

				throw TD.CreateHttpException(
					TD.HTTP_STATUS.INTERNAL_SERVER_ERROR,
					"Request failed",
					err.response || err,
				);
			}),
		);
	}
}
