import { Types } from "td-types";

export interface IRequestUser {
	id: number;
	role: Types.UserRole;
}
