import { RedisStore } from "connect-redis";
import { Session } from "express-session";
import { Request } from "express";

import { IRequestUser } from "@td/interfaces/request-user.interface";
import { TD } from "@td";

interface IAuthenticatedRequestObject {
	session: Session & TD.Types.SessionData;
	sessionStore: RedisStore;
	user: IRequestUser;
}

export type IRequest = IAuthenticatedRequestObject & Request;
