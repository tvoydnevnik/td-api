import { HttpException } from "@nestjs/common";
import { Observable, of } from "rxjs";
import { Types } from "td-types";

/**
 * Generic form of a response.
 */
export class CommonResponse<DataT> implements Types.IHttpResponse<DataT> {
	message = "Success";
	status = 200;
	data?: DataT;

	constructor(initializer: Partial<CommonResponse<DataT>>) {
		Object.assign(this, initializer);
	}
}

export type EndpointResponse<T> = Observable<CommonResponse<T>>;

export function CreateEndpointResponse<T>(
	data: T,
	status = 200,
	message = "OK",
): EndpointResponse<T> {
	if (status === 200)
		return of(
			new CommonResponse<T>({
				data: data,
				status: status,
				message: message,
			}),
		);
	else throw CreateHttpException(status, message);
}

export function CreateHttpException(
	status: number,
	message: string,
	data?: any,
): HttpException {
	return new HttpException(
		new CommonResponse({ data: data, status: status, message: message }),
		status,
	);
}
