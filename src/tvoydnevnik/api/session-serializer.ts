import { PassportSerializer } from "@nestjs/passport";
import { Injectable } from "@nestjs/common";
import { Types } from "td-types";

@Injectable()
export class SessionSerializer extends PassportSerializer {
	async serializeUser(
		userEntity: Promise<Types.IUser>,
		done: (error: any, payload: Types.IUser) => void,
	): Promise<void> {
		const user = await userEntity;
		done(null, user);
	}

	deserializeUser(
		payload: Types.IUser,
		done: (error: any, user: Types.IUser) => void,
	): void {
		done(null, payload);
	}
}
