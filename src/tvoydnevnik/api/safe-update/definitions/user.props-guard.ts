import { SafeUpdateOptions } from "../safe-update";
import { Types } from "td-types";

export const User: SafeUpdateOptions<Types.IUser> = {
	username: true,
	email: true,
	personal_info: true,
	contacts: true,
};
