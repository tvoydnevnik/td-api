import { TD } from "@td";

/**
 * @template T
 * Converts value types of T into boolean.
 * Each value represents whether its key on the source type is allowed to be modified via SafeUpdate function.
 * @see SafeUpdate
 * @see SafeUpdateConstraints
 */
export type SafeUpdateOptions<T> = Partial<{ [K in keyof T]: boolean }>;

export class SafeUpdateConstraints {
	/**
	 * SafeUpdate options.
	 * @see SafeUpdateOptions
	 * @private
	 */
	private readonly constraints: SafeUpdateOptions<any>;
	/**
	 * The user that is currently trying to modify a particular object.
	 * @private
	 */
	private readonly user?: TD.IRequestUser;
	/**
	 * Users with this role will be able to modify objects independent of their SafeUpdate constraints.
	 * @private
	 */
	private readonly managementRole?: TD.Types.UserRole;

	constructor(
		constraints: SafeUpdateOptions<any>,
		user?: TD.IRequestUser,
		managementRole = TD.Types.UserRole.ADMIN,
	) {
		this.constraints = constraints;
		this.user = user;
		this.managementRole = managementRole;
	}

	public GetConstraint(key: string): boolean | undefined {
		return this.constraints[key];
	}

	public CheckByRole(): boolean {
		return this.user?.role >= this.managementRole;
	}
}

/**
 * Constructs SafeUpdateConstraints from the given config.
 * @param config SafeUpdateConstraints constructor arguments in an object
 * @return New instance of SafeUpdateConstraints
 * @see SafeUpdateConstraints
 */
export function BuildConstraints(config: {
	constraints: SafeUpdateOptions<any>;
	user?: TD.IRequestUser;
	managementRole?: TD.Types.UserRole;
}): SafeUpdateConstraints {
	return new SafeUpdateConstraints(
		config.constraints,
		config.user,
		config.managementRole,
	);
}

/**
 * Takes in target object and its update constraints,
 * applies payload on the target object according to constraints.
 * The function is pure and does not modify source object.
 * @param c Object defining whether a property should be changed or not.
 * @param source Source object to operate on or an empty object to set properties on
 * @param payload Payload that is applied on target object
 * @returns Updated target
 * @see SafeUpdateOptions
 */
export function SafeUpdate<T>(
	c: SafeUpdateConstraints,
	source: T | { [k: string]: never },
	payload: Partial<T>,
): T {
	const tmp = source;
	Object.entries(payload).forEach(([k]) => {
		if (c.CheckByRole()) tmp[k] = payload[k];

		if (c.GetConstraint(k) === undefined) return;

		if (c.GetConstraint(k)) tmp[k] = payload[k];
	});

	return <T>tmp;
}
