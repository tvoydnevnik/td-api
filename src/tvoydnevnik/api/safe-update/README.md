# Entity prop guards

## What is it?

Entity prop guard is a concept that allows `SafeUpdate()` function to be used.

It defines which keys of the specified entity can be modified and which can not.

## How to write ones?

Entity prop guards have a very basic structure. You define a constant which keys are all of your entity's keys and
assign boolean values to each/some of those keys.

Here's an example:

```ts
/* your.props.guard.ts */

// Replace YourEntity with the one you're writing constraints for.
type UserEntityKeys = Partial<{ [K in keyof YourEntity]: boolean }>;

// If you leave any of the keys undefined,
// they will automatically count as non-editable (falsy)
export const YourEntityConstraints: UserEntityKeys = {
	editable_key: true,
	you_cant_edit_this_one: false,
	personal_info: true,
};

/* your.service.ts */
import { YourEntityConstraints } from "your.props.guard.ts";

let entity: YourEntity = {
	editable_key: "hey! you can edit me.",
	you_cant_edit_this_one: "you cant!",
	personal_info: { phone: "mobile" },
	not_listed_key: "I wasnt listed =(",
};

let payload: Partial<YourEntity> = {
	editable_key: "bla-bla-bla",
	you_cant_edit_this_one: "can't I?!",
	personal_info: { phone: "work" },
	not_listed_key: "Who cares?",
};

function someFunction(
	entity: YourEntity,
	payload: Partial<YourEntity>,
): Observable<YourEntity> {
	const updated = SafeUpdate(YourEntityConstraints, entity, payload);
	console.log(updated);
}

someFunction(entity, payload);
//  YourEntity: {
//      editable_key: "bla-bla-bla",
//      you_cant_edit_this_one: "you cant!",
//      personal_info: { phone: "work" },
//      not_listed_key: "I wasnt listed =("
//  }
```
