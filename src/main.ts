import { ExceptionInterceptor } from "@td/interceptors/exception.interceptor";
import { ValidationPipe } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import * as compression from "compression";
import * as session from "express-session";
import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import * as passport from "passport";
import { createClient } from "redis";
import helmet from "helmet";
import { TD } from "@td";

// eslint-disable-next-line @typescript-eslint/no-var-requires
const RedisStore = require("connect-redis")(session);

declare module "express-session" {
	interface SessionData extends TD.Types.SessionData {
		id: string;
		passport: { user: TD.Types.IUser };
		device_info: TD.Types.SessionDeviceData;
	}
}

async function bootstrap() {
	const app = await NestFactory.create(AppModule);

	const configService: ConfigService = app.get(ConfigService);

	const redisConnectionUrl = configService.get<string>("REDIS_URL");
	const sessionSecret = configService.get<string>("SESSION_SECRET");

	const redisClient = createClient({
		url: redisConnectionUrl,
		legacyMode: true,
	});
	await redisClient.connect();

	app.use(helmet());

	app.use(
		session({
			store: new RedisStore({
				client: redisClient,
				prefix: "td-session:",
			}),
			secret: sessionSecret,
			saveUninitialized: false,
			// Renews session expiration date on each request
			resave: true,
			rolling: true,
			name: "td.session",
			cookie: {
				// Cookies live for 3 days
				// unless renewed when making a request to the server
				maxAge:
					3 /* days */ *
					24 /* hours */ *
					60 /* minutes */ *
					60 /* seconds */ *
					1000 /* millis */,
				secure: process.env["NODE_ENV"] === "production",
				sameSite: "strict",
			},
		}),
	);

	app.use(passport.initialize());
	app.use(passport.session());

	app.use(compression());

	app.enableCors({
		credentials: true,
		origin: true,
	});

	app.useGlobalPipes(
		new ValidationPipe({
			transform: true,
			transformOptions: { enableImplicitConversion: true },
		}),
	);
	app.useGlobalInterceptors(new ExceptionInterceptor());
	app.enableShutdownHooks();

	await app.listen(3000);
}

bootstrap();
