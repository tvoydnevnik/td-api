import { Injectable } from "@nestjs/common";

import { from, map, mergeMap, Observable, of } from "rxjs";

import { PrismaService } from "@td-prisma";
import { Prisma } from "@prisma/client";

import { GenerateTimetable } from "./generate-timetable";
import { TD } from "@td";

@Injectable()
export class TimetableService {
	constructor(private readonly prisma: PrismaService) {}

	GetTimetablePeriodFilter(
		request: TD.Types.ITimetableRequest["request_opts"],
	): Observable<Prisma.QuarterWeekWhereInput> {
		if (request.for_week)
			return from(this.prisma.extended.quarterWeek.findCurrent()).pipe(
				map((qw) => ({ id: qw.id })),
			);

		if (request.for_quarter)
			return from(this.prisma.extended.quarter.findCurrent()).pipe(
				map((q) => ({
					quarter_id: q.id,
				})),
			);

		if (request.for_year)
			return from(this.prisma.extended.schoolYear.findCurrent()).pipe(
				map((y) => ({ quarter: { school_year_id: y.id } })),
			);

		return of({});
	}

	GetTimetable(
		contextUser?: TD.IRequestUser,
		request?: TD.Types.ITimetableRequest,
	): Observable<TD.Types.ITimetableResponse> {
		if (!request)
			throw TD.CreateHttpException(
				TD.HTTP_STATUS.BAD_REQUEST,
				"No timetable request supplied. Try appending 'request' query to the endpoint URL",
			);

		const { request_opts, filtering_opts } = request;

		const { with_diary } = request_opts ?? {};

		if (with_diary && !contextUser)
			throw TD.CreateHttpException(
				TD.HTTP_STATUS.FORBIDDEN,
				"Unauthorized users cannot request diary alongside timetable",
			);

		const {
			form_id,
			student_id,
			user_id,
			teacher_id,
			lesson_id,
			form_lesson_id,
			quarter_id,
			quarter_week_id,
			quarter_week_index,
			school_year_id,
			subgroup_id,
		} = filtering_opts ?? {};

		return this.GetTimetablePeriodFilter(request_opts).pipe(
			mergeMap((period_filter) =>
				this.prisma.timetable.findMany({
					where: {
						form_lesson: {
							id: form_lesson_id,
							teacher_id: teacher_id ?? undefined,
							lesson_id: lesson_id ?? undefined,
							form_subgroup_id: subgroup_id ? subgroup_id : undefined,
							OR: !subgroup_id
								? [
										{
											form_subgroup: {
												students: {
													some: { id: student_id, user_id: user_id },
												},
											},
										},
										{ form_subgroup_id: null },
								  ]
								: undefined,
						},
						form_id: form_id ?? undefined,
						form: {
							students: {
								some: {
									id: student_id ?? undefined,
									user_id: user_id ?? undefined,
								},
							},
						},
						quarter_week: {
							id: quarter_week_id ?? undefined,
							index: quarter_week_index ?? undefined,
							quarter: {
								id: quarter_id ?? undefined,
								school_year_id: school_year_id ?? undefined,
							},
							...period_filter,
						},
					},
					include: {
						form_lesson: {
							include: {
								form: false,
								timetable: false,
								form_subgroup: true,
								lesson: true,
								teacher: true,
							},
						},
						quarter_week: true,
						form: true,
						diary: with_diary
							? {
									where: {
										student: {
											id: student_id,
											OR:
												// If user is less privileged than teacher, then only student's parents can access their diary
												contextUser?.role < TD.Types.UserRole.TEACHER
													? [
															{
																parents: { some: { user_id: contextUser.id } },
															},
															{ user_id: contextUser.id },
													  ]
													: undefined,
										},
									},
									include: {
										student: { include: { user: true } },
										user: true,
									},
							  }
							: undefined,
					},
					orderBy: [
						{ quarter_week: { start_date: "asc" } },
						{ weekday: "asc" },
						{ index: "asc" },
					],
				}),
			),
			map((t) => ({
				start_date: t.at(0)?.quarter_week?.start_date?.toISOString(),
				end_date: t.at(-1)?.quarter_week?.end_date?.toISOString(),
				timetable: t,
			})),
		);
	}

	FillTimetable(
		data: TD.Types.ITimetablePayload,
	): Observable<TD.Types.ITimetableEntry[]> {
		return from(GenerateTimetable(data, this.prisma)).pipe(
			mergeMap((t) =>
				this.prisma.$transaction(
					t.map((v) =>
						this.prisma.timetable.create({
							data: {
								index: v.index,
								weekday: v.weekday,
								form_lesson: v.form_lesson,
								quarter_week: v.quarter_week,
								start_time: v.start_time,
								end_time: v.end_time,
								classroom: v.classroom,
								form: v.form,
							},
						}),
					),
				),
			),
		);
	}

	UpdateBatch(
		payload: TD.Types.DeepPartial<TD.Types.ITimetableEntry>[],
	): Observable<boolean> {
		return from(
			this.prisma.$transaction(
				payload.map((v) =>
					this.prisma.timetable.update({
						where: { id: v.id },
						data: {
							start_time: v.start_time,
							end_time: v.end_time,
							homework: v.homework,
							lesson_topic: v.lesson_topic,
							weekday: v.weekday,
							index: v.index,
							classroom: v.classroom,
						},
					}),
				),
			),
		).pipe(map(() => true));
	}

	UpdateOne(
		id: number,
		payload: TD.Types.DeepPartial<TD.Types.ITimetableEntry>,
	): Observable<TD.Types.ITimetableEntry> {
		return from(
			this.prisma.timetable.update({
				where: { id },
				data: {
					start_time: payload.start_time,
					end_time: payload.end_time,
					homework: payload.homework,
					lesson_topic: payload.lesson_topic,
					weekday: payload.weekday,
					index: payload.index,
					classroom: payload.classroom,
				},
			}),
		);
	}
}
