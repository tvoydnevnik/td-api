import { TimetableService } from "./timetable.service";
import { mergeMap } from "rxjs";
import {
	Body,
	Controller,
	Get,
	Param,
	Patch,
	Post,
	Request,
	UseGuards,
} from "@nestjs/common";
import { TD } from "@td";

@Controller("timetable")
export class TimetableController {
	constructor(private readonly timetableService: TimetableService) {}

	@UseGuards(TD.AuthenticatedGuard)
	@Get()
	GetTimetable(
		@TD.RequestQuery({ validator: TD.Types.ITimetableRequest, throw: true })
		request_query: TD.Types.ITimetableRequest,
		@Request() req: TD.IRequest,
	): TD.EndpointResponse<TD.Types.ITimetableResponse> {
		return this.timetableService
			.GetTimetable(req.user, request_query)
			.pipe(mergeMap((t) => TD.CreateEndpointResponse(t)));
	}

	@UseGuards(TD.AuthenticatedGuard)
	@Get("mine")
	GetTimetableMine(
		@TD.RequestQuery({ validator: TD.Types.ITimetableRequest, throw: true })
		request_query: TD.Types.ITimetableRequest,
		@Request() req: TD.IRequest,
	): TD.EndpointResponse<TD.Types.ITimetableResponse> {
		return this.timetableService
			.GetTimetable(req.user, {
				...request_query,
				filtering_opts: {
					...request_query.filtering_opts,
					user_id: req.user.id,
				},
			})
			.pipe(mergeMap((t) => TD.CreateEndpointResponse(t)));
	}

	@TD.MinimalRole(TD.Types.UserRole.TEACHER)
	@Post("fill")
	FillTimetable(
		@Body() body: TD.Types.ITimetablePayload,
	): TD.EndpointResponse<boolean> {
		return this.timetableService
			.FillTimetable(body)
			.pipe(mergeMap(() => TD.CreateEndpointResponse(true)));
	}

	@TD.MinimalRole(TD.Types.UserRole.TEACHER)
	@Patch()
	UpdateBatch(
		@Body() body: Partial<TD.Types.ITimetableEntry>[],
	): TD.EndpointResponse<boolean> {
		return this.timetableService
			.UpdateBatch(body)
			.pipe(mergeMap((r) => TD.CreateEndpointResponse(r)));
	}

	@TD.MinimalRole(TD.Types.UserRole.TEACHER)
	@Patch(":id")
	UpdateEntry(
		@Param("id") id: number,
		@Body() body: Partial<TD.Types.ITimetableEntry>,
	): TD.EndpointResponse<TD.Types.ITimetableEntry> {
		return this.timetableService
			.UpdateOne(id, body)
			.pipe(mergeMap((entry) => TD.CreateEndpointResponse(entry)));
	}
}
