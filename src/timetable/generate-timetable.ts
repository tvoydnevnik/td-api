import { Prisma, PrismaClient } from "@prisma/client";
import { DateTime } from "luxon";
import { TD } from "@td";

export async function GenerateTimetable(
	data: TD.Types.ITimetablePayload,
	prisma: PrismaClient,
): Promise<Prisma.TimetableCreateInput[]> {
	const weeks = await prisma.quarterWeek.findMany({
		where: { quarter_id: data.quarter },
		include: { quarter: true },
	});
	const timetable: Prisma.TimetableCreateInput[] = [];

	for (const week of weeks) {
		const week_start = DateTime.fromJSDate(week.start_date);
		const week_end = DateTime.fromJSDate(week.end_date);

		let iteration_date = week_start;
		while (iteration_date <= week_end) {
			for (const entry of data.payload) {
				if (entry.weekday !== iteration_date.weekday - 1) continue;

				timetable.push({
					quarter_week: { connect: { id: week.id } },
					weekday: entry.weekday,
					form_lesson: {
						connectOrCreate: {
							create: {
								form_id: data.form,
								lesson_id: entry.lesson,
								teacher_id: entry.teacher,
								form_subgroup_id: entry.form_subgroup,
								school_year_id: week.quarter.school_year_id,
							},
							where: entry.form_subgroup
								? {
										unique_subgroup: {
											form_id: data.form,
											lesson_id: entry.lesson,
											teacher_id: entry.teacher,
											form_subgroup_id: entry.form_subgroup,
										},
								  }
								: {
										unique: {
											form_id: data.form,
											lesson_id: entry.lesson,
											teacher_id: entry.teacher,
										},
								  },
						},
					},
					index: entry.index,
					start_time: entry.start_time,
					end_time: entry.end_time,
					classroom: entry.classroom,
					form: { connect: { id: data.form } },
				});
			}
			iteration_date = iteration_date.plus({ days: 1 });
		}
	}

	return timetable;
}
