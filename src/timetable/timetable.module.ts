import { QuartersModule } from "../quarters/quarters.module";
import { TimetableController } from "./timetable.controller";
import { TimetableService } from "./timetable.service";
import { Module } from "@nestjs/common";

@Module({
	imports: [QuartersModule],
	controllers: [TimetableController],
	providers: [TimetableService],
	exports: [QuartersModule, TimetableService],
})
export class TimetableModule {}
