import { Controller, Get } from "@nestjs/common";
import { TD } from "@td";

@Controller()
export class AppController {
	@Get("hello")
	Greetings(): TD.EndpointResponse<string> {
		return TD.CreateEndpointResponse(
			"Hello, visitor! This is the root node of TvoyDnevnik API!",
		);
	}
}
