import { FinalMarksController } from "./final-marks.controller";
import { FinalMarksService } from "./final-marks.service";
import { Module } from "@nestjs/common";

@Module({
	controllers: [FinalMarksController],
	providers: [FinalMarksService],
})
export class FinalMarksModule {}
