import { Injectable } from "@nestjs/common";

import { forkJoin, iif, map, mergeMap, Observable, of } from "rxjs";

import { PrismaService } from "@td-prisma";

import { TD } from "@td";

@Injectable()
export class FinalMarksService {
	constructor(private readonly prisma: PrismaService) {}

	GetFinal(
		request: TD.Types.IFinalMarksRequest,
	): Observable<TD.Types.IFinalMarksResponse> {
		const { filtering_opts, request_opts } = request;
		const { student_id, form_id, form_lesson_id, quarter_id, school_year_id } =
			filtering_opts;
		const { for_year, for_quarter } = request_opts;

		const filters = {
			student: { id: student_id, form_id: form_id },
			form_lesson: { id: form_lesson_id },
		};

		return forkJoin({
			current_quarter: iif(
				() => for_quarter,
				this.prisma.extended.quarter.findCurrent(),
				of(undefined),
			),
			current_year: iif(
				() => for_year,
				this.prisma.extended.schoolYear.findCurrent(),
				of(undefined),
			),
		}).pipe(
			mergeMap(({ current_quarter, current_year }) =>
				forkJoin({
					school_year: school_year_id
						? this.prisma.schoolYear.findUnique({
								where: { id: school_year_id },
						  })
						: this.prisma.schoolYear.findUnique({ where: { active: true } }),
					quarterly: this.prisma.quarterlyMark.findMany({
						where: {
							...filters,
							quarter: quarter_id
								? { id: quarter_id }
								: school_year_id
								? { school_year_id: school_year_id }
								: for_quarter
								? { id: current_quarter.id }
								: for_year
								? { school_year_id: current_year.id }
								: undefined,
						},
					}),
					yearly: this.prisma.yearlyMark.findMany({
						where: {
							...filters,
							school_year: school_year_id
								? { id: school_year_id }
								: for_year
								? { id: current_year.id }
								: undefined,
						},
					}),
				}),
			),
		);
	}

	PostFinal(payload: TD.Types.IFinalMarksPayload): Observable<boolean> {
		return forkJoin({
			quarterly: this.prisma.$transaction(
				payload.quarterly_marks.map((v) =>
					this.prisma.quarterlyMark.upsert({
						where: {
							unique: {
								quarter_id: v.quarter_id,
								form_lesson_id: v.form_lesson_id,
								student_id: v.student_id,
							},
						},
						create: {
							user_id: payload.user_id,
							form_lesson_id: v.form_lesson_id,
							student_id: v.student_id,
							quarter_id: v.quarter_id,
							mark: v.mark,
						},
						update: {
							mark: v.mark,
						},
					}),
				),
			),
			yearly: this.prisma.$transaction(
				payload.yearly_marks.map((v) =>
					this.prisma.yearlyMark.upsert({
						where: {
							unique: {
								school_year_id: v.school_year_id,
								form_lesson_id: v.form_lesson_id,
								student_id: v.student_id,
							},
						},
						create: {
							form_lesson_id: v.form_lesson_id,
							student_id: v.student_id,
							school_year_id: v.school_year_id,
							mark: v.mark,
						},
						update: {
							mark: v.mark,
						},
					}),
				),
			),
		}).pipe(map(() => true));
	}
}
