import { Test, TestingModule } from "@nestjs/testing";
import { FinalMarksService } from "./final-marks.service";

describe("FinalMarksService", () => {
	let service: FinalMarksService;

	beforeEach(async () => {
		const module: TestingModule = await Test.createTestingModule({
			providers: [FinalMarksService],
		}).compile();

		service = module.get<FinalMarksService>(FinalMarksService);
	});

	it("should be defined", () => {
		expect(service).toBeDefined();
	});
});
