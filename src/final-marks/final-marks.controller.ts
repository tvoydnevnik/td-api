import { RequestQuery } from "@td/decorators/request-query.decorator";
import { Body, Controller, Get, Post } from "@nestjs/common";
import { FinalMarksService } from "./final-marks.service";
import { mergeMap } from "rxjs";
import { TD } from "@td";

@Controller("final-marks")
export class FinalMarksController {
	constructor(private readonly finalMarksService: FinalMarksService) {}

	@Get()
	GetFinal(
		@RequestQuery({ validator: TD.Types.IFinalMarksRequest, throw: true })
		r: TD.Types.IFinalMarksRequest,
	): TD.EndpointResponse<TD.Types.IFinalMarksResponse> {
		return this.finalMarksService
			.GetFinal(r)
			.pipe(mergeMap((m) => TD.CreateEndpointResponse(m)));
	}

	@TD.MinimalRole(TD.Types.UserRole.TEACHER)
	@Post()
	PostFinal(
		@Body() payload: TD.Types.IFinalMarksPayload,
	): TD.EndpointResponse<boolean> {
		return this.finalMarksService
			.PostFinal(payload)
			.pipe(mergeMap((m) => TD.CreateEndpointResponse(m)));
	}
}
