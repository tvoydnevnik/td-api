import { Injectable } from "@nestjs/common";

import { from, map, mergeMap, Observable, tap } from "rxjs";

import { UsersService } from "../users/users.service";

import { PrismaService } from "@td-prisma";
import { Prisma } from "@prisma/client";

import { randomBytes } from "crypto";

import { IRequestUser } from "@td/interfaces/request-user.interface";
import { RegistrationDTO } from "./dtos/registration.dto";
import { TD } from "@td";

@Injectable()
export class AuthService {
	constructor(
		private readonly prisma: PrismaService,
		private readonly usersService: UsersService,
	) {}

	Register(payload: RegistrationDTO): Observable<TD.Types.IUser> {
		return this.VerifyInvitationalCode(payload.code).pipe(
			mergeMap((user) => {
				const { username, email, password } = payload;

				return this.usersService.UpdateOne(undefined, user.id, {
					username,
					email,
					password,
				});
			}),
			mergeMap((user) => this.usersService.CompleteRegistration(user)),
		);
	}

	PreRegister(
		registrar: IRequestUser,
		payload: TD.Types.IUserRegistrationPayload,
	): Observable<TD.Types.IUser> {
		const { last_name, first_name, patronymic, role } = payload;

		// check if role is correct
		if (!TD.Types.GetRoleString(role))
			throw TD.CreateHttpException(
				TD.HTTP_STATUS.BAD_REQUEST,
				"Invalid role value",
			);

		// validate privileges
		if (role >= registrar.role)
			throw TD.CreateHttpException(
				TD.HTTP_STATUS.FORBIDDEN,
				"You can't create users privileged higher or the same as you",
			);

		const createInput: Prisma.UserCreateInput = {
			last_name,
			first_name,
			patronymic,
			role,
		};
		createInput[TD.Types.GetRoleString(role)] = { create: {} };

		return from(
			this.prisma.user.create({ data: createInput }),
		) as Observable<TD.Types.IUser>;
	}

	GenerateInvitationalCode(user_id: number): Observable<string> {
		const code = randomBytes(12).toString("hex");

		return from(this.usersService.GetById(user_id)).pipe(
			mergeMap((user) => {
				if (user.is_registered)
					throw TD.CreateHttpException(
						TD.HTTP_STATUS.CONFLICT,
						"User already registered",
					);

				return this.prisma.registrationQueue.create({
					data: { user_id: user.id, code: code },
				});
			}),
			map((r) => r.code),
		);
	}

	VerifyInvitationalCode(code: string): Observable<TD.Types.IUser> {
		return from(
			this.prisma.registrationQueue.findUnique({ where: { code } }).user(),
		).pipe(
			tap((q) => {
				if (!q)
					throw TD.CreateHttpException(
						TD.HTTP_STATUS.BAD_REQUEST,
						"Invalid invitation code",
					);
			}),
		) as Observable<TD.Types.IUser>;
	}
}
