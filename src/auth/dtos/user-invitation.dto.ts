import { IsNotEmpty, IsNumber } from "class-validator";

export class InvitationDTO {
	@IsNumber()
	@IsNotEmpty()
	user: number;
}
