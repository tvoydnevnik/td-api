import { IsEmail, IsNotEmpty, IsString, Length } from "class-validator";

export class RegistrationDTO {
	@IsString()
	@Length(24, 24)
	@IsNotEmpty()
	code: string;

	@IsString()
	@IsNotEmpty()
	username: string;

	@IsString()
	@IsEmail()
	email: string;

	@IsString()
	@IsNotEmpty()
	password: string;
}
