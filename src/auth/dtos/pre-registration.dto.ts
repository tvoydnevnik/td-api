import {
	IsEnum,
	IsNotEmpty,
	IsNotEmptyObject,
	IsOptional,
	IsString,
} from "class-validator";

import { TD } from "@td";

export class PreRegistrationDTO implements TD.Types.IUserRegistrationPayload {
	@IsString()
	@IsNotEmpty()
	first_name: string;

	@IsString()
	@IsNotEmpty()
	last_name: string;

	@IsString()
	@IsOptional()
	patronymic?: string;

	@IsEnum(TD.Types.UserRole)
	@IsNotEmptyObject()
	role: TD.Types.UserRole;
}
