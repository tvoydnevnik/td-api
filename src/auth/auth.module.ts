import { UsersModule } from "../users/users.module";
import { AuthController } from "./auth.controller";
import { PassportModule } from "@nestjs/passport";
import { AuthService } from "./auth.service";
import { Module } from "@nestjs/common";
import { TD } from "@td";

@Module({
	imports: [UsersModule, PassportModule.register({ session: true })],
	providers: [AuthService, TD.LocalPassportStrategy, TD.SessionSerializer],
	controllers: [AuthController],
})
export class AuthModule {}
