import {
	Body,
	Controller,
	Get,
	Param,
	Post,
	Request,
	UseGuards,
} from "@nestjs/common";

import { catchError, mergeMap, Subject } from "rxjs";

import { RegistrationDTO } from "./dtos/registration.dto";
import { PreRegistrationDTO } from "./dtos/pre-registration.dto";
import { InvitationDTO } from "./dtos/user-invitation.dto";
import { BuildSessionDeviceInfo } from "./device-info/session-device-info-builder";

import { AuthService } from "./auth.service";

import { IRequestUser } from "@td/interfaces/request-user.interface";
import { IRequest } from "@td/interfaces/request.interface";
import { TD } from "@td";

@Controller("auth")
export class AuthController {
	constructor(private readonly authService: AuthService) {}

	@Get("validate")
	ValidateSession(@Request() req: IRequest): TD.EndpointResponse<boolean> {
		const session = req.session;
		const store = req.sessionStore;

		const callback$ = new Subject<boolean>();
		store.get(session.id, (err, session) => {
			if (err) {
				callback$.error(err);
				callback$.complete();
				return;
			}

			callback$.next(!!session);
			callback$.complete();
		});

		return callback$.pipe(
			mergeMap((valid) => TD.CreateEndpointResponse(valid)),
		);
	}

	@TD.MinimalRole(TD.Types.UserRole.TEACHER)
	@Post("pre-register")
	PreRegister(
		@Request() req: IRequest,
		@Body() body: PreRegistrationDTO,
	): TD.EndpointResponse<TD.Types.IUser> {
		return this.authService
			.PreRegister(req.user, body)
			.pipe(mergeMap((u) => TD.CreateEndpointResponse(u)));
	}

	@TD.MinimalRole(TD.Types.UserRole.TEACHER)
	@Post("invite")
	InviteUser(@Body() body: InvitationDTO): TD.EndpointResponse<string> {
		return this.authService
			.GenerateInvitationalCode(body.user)
			.pipe(mergeMap((code) => TD.CreateEndpointResponse(code)));
	}

	@Post("register")
	Register(@Body() body: RegistrationDTO): TD.EndpointResponse<TD.Types.IUser> {
		return this.authService
			.Register(body)
			.pipe(mergeMap((u) => TD.CreateEndpointResponse(u)));
	}

	@UseGuards(TD.LoginGuard)
	@Post("login")
	Login(@Request() req: IRequest): TD.EndpointResponse<IRequestUser> {
		const currentSession = req.session;
		const sessionStore = req.sessionStore;

		currentSession.device_info = BuildSessionDeviceInfo(req.get("user-agent"));

		const callback$ = new Subject<void>();
		sessionStore.set(currentSession.id, currentSession, (err) => {
			if (err) callback$.error(err);
			else callback$.next();

			callback$.complete();
		});

		return callback$.pipe(
			mergeMap(() =>
				TD.CreateEndpointResponse(req.user, TD.HTTP_STATUS.OK, "Authorized"),
			),
		);
	}

	@UseGuards(TD.AuthenticatedGuard)
	@Get("sessions")
	GetMySessions(
		@Request() req: TD.IRequest,
	): TD.EndpointResponse<TD.Types.SessionData[]> {
		const store = req.sessionStore;
		const { user } = req;

		const callback$ = new Subject<TD.Types.SessionData[]>();
		store.all((err, data) => {
			if (err) callback$.error(err);

			const sessions = (
				Array.isArray(data) ? data : Object.values(data)
			).filter((session) => session.passport.user.id === user.id);

			callback$.next(sessions);
			callback$.complete();
		});

		return callback$.pipe(mergeMap((s) => TD.CreateEndpointResponse(s)));
	}

	@UseGuards(TD.AuthenticatedGuard)
	@Post("logout")
	Logout(@Request() req: IRequest): TD.EndpointResponse<boolean> {
		const callback$ = new Subject<void>();
		req.session.destroy((err) => {
			if (err) callback$.error(err);
			else callback$.next();

			callback$.complete();
		});

		return callback$.pipe(
			mergeMap(() =>
				TD.CreateEndpointResponse(true, TD.HTTP_STATUS.OK, "Logged out"),
			),
		);
	}

	@UseGuards(TD.AuthenticatedGuard)
	@Post("logout/:sid")
	LogoutSession(
		@Param("sid") sid: string,
		@Request() req: IRequest,
	): TD.EndpointResponse<boolean> {
		const sessionStore = req.sessionStore;
		const user = req.user;

		const callback$ = new Subject<void>();
		sessionStore.get(sid, (err, data) => {
			if (err) {
				callback$.error(err);
				callback$.complete();
				return;
			}

			if (user.id !== data.passport.user.id) {
				callback$.error({
					message: "Session does not belong to current user.",
					unauthorized: true,
				});
				callback$.complete();
				return;
			}

			sessionStore.destroy(sid, (err) => {
				if (err) callback$.error(err);
				else callback$.next();

				callback$.complete();
			});
		});

		return callback$.pipe(
			mergeMap(() =>
				TD.CreateEndpointResponse(
					true,
					TD.HTTP_STATUS.OK,
					"Session logged out",
				),
			),
			catchError((e) => {
				throw e.unauthorized
					? TD.CreateHttpException(TD.HTTP_STATUS.UNAUTHORIZED, e.message)
					: e;
			}),
		);
	}
}
