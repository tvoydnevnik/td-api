import * as DeviceDetector from "device-detector-js";
import { TD } from "@td";

export function BuildSessionDeviceInfo(
	userAgent: string,
): TD.Types.SessionDeviceData {
	const detectorResult = new DeviceDetector().parse(userAgent);
	const deviceData: TD.Types.SessionDeviceData = {
		os: "",
		client: "",
		device: "",
	};

	deviceData.device = detectorResult.device
		? `${detectorResult.device?.brand || ""} ${
				detectorResult.device?.type || ""
		  } ${
				detectorResult.device?.model ? `(${detectorResult.device?.model})` : ""
		  }`
		: "";

	deviceData.os = detectorResult.os
		? `${detectorResult.os?.name || ""} ${detectorResult.os?.version || ""} ${
				detectorResult.os?.platform ? `(${detectorResult.os?.platform})` : ""
		  }`
		: "";

	deviceData.client = detectorResult.client
		? `${detectorResult.client?.name || ""} ${
				detectorResult.client?.version || ""
		  }`
		: "";

	return deviceData;
}
