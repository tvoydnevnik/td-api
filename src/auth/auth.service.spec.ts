import { Test, TestingModule } from "@nestjs/testing";
import { UsersService } from "../users/users.service";
import { AuthService } from "./auth.service";
import { DataSource } from "typeorm";
import { TD } from "@td";

describe("AuthService", () => {
	let service: AuthService;
	let dataSource: DataSource;

	beforeEach(async () => {
		dataSource = await TD.Testing.CreateDataSource();

		const module: TestingModule = await Test.createTestingModule({
			providers: [
				AuthService,
				UsersService,
				TD.Testing.CreateCustomProvider(
					TD.UserEntity,
					dataSource.getRepository(TD.UserEntity),
				),
			],
		}).compile();

		service = module.get<AuthService>(AuthService);
	});

	it("should be defined", () => {
		expect(service).toBeDefined();
	});
});
