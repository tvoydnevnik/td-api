import { Body, Controller, Delete, Get, Param, Patch } from "@nestjs/common";

import { mergeMap } from "rxjs";

import { ParentsService } from "./parents.service";

import { TD } from "@td";

@Controller("parents")
export class ParentsController {
	constructor(private readonly parentsService: ParentsService) {}

	@Get()
	GetAll(): TD.EndpointResponse<TD.Types.IParent[]> {
		return this.parentsService
			.GetAll()
			.pipe(mergeMap((parents) => TD.CreateEndpointResponse(parents)));
	}

	@Get(":id")
	GetById(@Param("id") id: number): TD.EndpointResponse<TD.Types.IParent> {
		return this.parentsService
			.GetById(id)
			.pipe(mergeMap((parent) => TD.CreateEndpointResponse(parent)));
	}

	@TD.MinimalRole(TD.Types.UserRole.TEACHER)
	@Patch(":id")
	UpdateOne(
		@Param("id") id: number,
		@Body() body: TD.Types.DeepPartial<TD.Types.IParent>,
	): TD.EndpointResponse<TD.Types.IParent> {
		return this.parentsService
			.UpdateOne(id, body)
			.pipe(mergeMap((parent) => TD.CreateEndpointResponse(parent)));
	}

	@TD.MinimalRole(TD.Types.UserRole.TEACHER)
	@Delete(":id")
	DeleteOne(@Param("id") id: number): TD.EndpointResponse<TD.Types.IParent> {
		return this.parentsService
			.DeleteOne(id)
			.pipe(mergeMap((res) => TD.CreateEndpointResponse(res)));
	}
}
