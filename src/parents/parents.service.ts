import { Injectable } from "@nestjs/common";

import { from, Observable, tap } from "rxjs";

import { PrismaService } from "@td-prisma";

import { TD } from "@td";

@Injectable()
export class ParentsService {
	constructor(private readonly prisma: PrismaService) {}

	GetAll(): Observable<TD.Types.IParent[]> {
		return from(
			this.prisma.parent.findMany({ include: { user: true } }),
		) as Observable<TD.Types.IParent[]>;
	}

	GetById(id: number): Observable<TD.Types.IParent> {
		return from(
			this.prisma.parent.findUnique({
				where: { id },
				include: {
					user: true,
					students: {
						include: { user: true },
						orderBy: [
							{ form: { grade: "asc" } },
							{ form: { letter: "asc" } },
							{ user: { last_name: "asc" } },
							{ user: { first_name: "asc" } },
							{ user: { patronymic: "asc" } },
						],
					},
				},
			}),
		).pipe(
			tap((v) => {
				if (!v)
					throw TD.CreateHttpException(
						TD.HTTP_STATUS.NOT_FOUND,
						"Parent not found",
					);
			}),
		) as Observable<TD.Types.IParent>;
	}

	UpdateOne(
		id: number,
		payload: TD.Types.DeepPartial<TD.Types.IParent>,
	): Observable<TD.Types.IParent> {
		return from(
			this.prisma.parent.update({
				where: { id },
				data: {
					students: { set: payload.students?.map((v) => ({ id: v.id })) },
				},
			}),
		);
	}

	DeleteOne(id: number): Observable<TD.Types.IParent> {
		return from(this.prisma.parent.delete({ where: { id } }));
	}
}
