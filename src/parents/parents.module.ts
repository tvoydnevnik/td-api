import { ParentsController } from "./parents.controller";
import { ParentsService } from "./parents.service";
import { Module } from "@nestjs/common";

@Module({
	controllers: [ParentsController],
	providers: [ParentsService],
})
export class ParentsModule {}
