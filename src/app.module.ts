import { SchoolYearsModule } from "./school-years/school-years.module";
import { HeadmastersModule } from "./headmasters/headmasters.module";
import { FinalMarksModule } from "./final-marks/final-marks.module";
import { TimetableModule } from "./timetable/timetable.module";
import { QuartersModule } from "./quarters/quarters.module";
import { StudentsModule } from "./students/students.module";
import { TeachersModule } from "./teachers/teachers.module";
import { ParentsModule } from "./parents/parents.module";
import { LessonsModule } from "./lessons/lessons.module";
import { SchoolModule } from "./school/school.module";
import { PrismaModule } from "./prisma/prisma.module";
import { DiaryModule } from "./diary/diary.module";
import { UsersModule } from "./users/users.module";
import { FormsModule } from "./forms/forms.module";
import { AppController } from "./app.controller";
import { AuthModule } from "./auth/auth.module";
import { ConfigModule } from "@nestjs/config";
import { APP_GUARD } from "@nestjs/core";
import { Module } from "@nestjs/common";
import * as Joi from "joi";
import { TD } from "@td";

@Module({
	imports: [
		ConfigModule.forRoot({
			isGlobal: true,
			validationSchema: Joi.object({
				DATABASE_URL: Joi.string().required(),
				REDIS_URL: Joi.string().required(),
				SESSION_SECRET: Joi.string().required(),
			}),
		}),
		HeadmastersModule,
		SchoolYearsModule,
		FinalMarksModule,
		TimetableModule,
		QuartersModule,
		TeachersModule,
		StudentsModule,
		ParentsModule,
		LessonsModule,
		SchoolModule,
		PrismaModule,
		FormsModule,
		UsersModule,
		AuthModule,
		DiaryModule,
	],
	controllers: [AppController],
	providers: [
		{
			provide: APP_GUARD,
			useClass: TD.RolesGuard,
		},
	],
})
export class AppModule {}
