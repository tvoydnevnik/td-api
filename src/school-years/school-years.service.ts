import { Injectable } from "@nestjs/common";

import { from, map, Observable, tap } from "rxjs";

import { PrismaService } from "@td-prisma";

import { FillQuarterWeeks } from "../quarters/fill-quarter-weeks";
import { TD } from "@td";

@Injectable()
export class SchoolYearsService {
	constructor(private readonly prisma: PrismaService) {}

	GetAll(): Observable<TD.Types.ISchoolYear[]> {
		return from(
			this.prisma.schoolYear.findMany({ orderBy: { start_date: "asc" } }),
		);
	}

	GetCurrent(): Observable<TD.Types.ISchoolYear> {
		return from(
			this.prisma.schoolYear.findUnique({ where: { active: true } }),
		).pipe(
			tap((v) => {
				if (!v)
					throw TD.CreateHttpException(
						TD.HTTP_STATUS.NOT_FOUND,
						"Active school year not found",
					);
			}),
		);
	}

	GetById(id: number): Observable<TD.Types.ISchoolYear> {
		return from(this.prisma.schoolYear.findUnique({ where: { id } })).pipe(
			tap((v) => {
				if (!v)
					throw TD.CreateHttpException(
						TD.HTTP_STATUS.NOT_FOUND,
						"School year not found",
					);
			}),
		);
	}

	GetQuartersByYearId(id: number): Observable<TD.Types.IQuarter[]> {
		return from(
			this.prisma.quarter.findMany({
				where: { school_year_id: id },
				orderBy: { start_date: "asc" },
			}),
		);
	}

	CreateOne(
		payload: TD.Types.DeepPartial<TD.Types.ISchoolYear>,
	): Observable<TD.Types.ISchoolYear> {
		return from(
			this.prisma.extended.schoolYear.createWithActive({
				data: {
					start_date: new Date(payload.start_date),
					end_date: new Date(payload.end_date),
					active: payload.active,
				},
			}),
		);
	}

	CreateWithWizard(
		payload: TD.Types.ISchoolYearWizardPayload,
	): Observable<TD.Types.ISchoolYear> {
		return from(
			this.prisma.extended.$transaction(async (tx) => {
				const year_payload = payload.school_year;
				const y = await tx.schoolYear.createWithActive(
					{
						data: {
							start_date: new Date(year_payload.start_date),
							end_date: new Date(year_payload.end_date),
							active: year_payload.active,
						},
					},
					payload.supersede ? "supersede" : "update",
				);

				const quarters_payload = payload.quarters;
				// For each quarter payload create a quarter itself and fill weeks immediately.
				for (let i = 0; i < quarters_payload.length; i++) {
					const entry = quarters_payload[i];
					const next = quarters_payload[i + 1];
					const q = await tx.quarter.create({
						data: {
							name: entry.name,
							school_year_id: y.id,
							start_date: new Date(entry.start_date),
							end_date: new Date(entry.end_date),
							indexing_start: new Date(entry.start_date),
							indexing_end: new Date(next ? next.start_date : entry.end_date),
						},
					});

					// TODO: casting to any is not ideal, think of a workaround for this
					await FillQuarterWeeks(tx as any, q);
				}

				return y;
			}),
		);
	}

	Supersede(new_id: number): Observable<boolean> {
		return from(this.prisma.extended.schoolYear.supersede(new_id)).pipe(
			map(() => true),
		);
	}

	UpdateOne(
		id: number,
		payload: TD.Types.DeepPartial<TD.Types.ISchoolYear>,
	): Observable<TD.Types.ISchoolYear> {
		return from(
			this.prisma.extended.schoolYear.updateWithQuarters({
				where: { id },
				data: {
					start_date: new Date(payload.start_date),
					end_date: new Date(payload.end_date),
					active: payload.active,
				},
				include: { quarters: true },
			}),
		);
	}

	UpdateWithWizard(
		id: number,
		payload: TD.Types.IEditSchoolYearWizardPayload,
	): Observable<TD.Types.ISchoolYear> {
		return from(
			this.prisma.extended.$transaction(async (tx) => {
				const year_payload = payload.school_year;
				const y = await tx.schoolYear.updateWithActive(
					{
						where: { id },
						data: {
							start_date: new Date(year_payload.start_date),
							end_date: new Date(year_payload.end_date),
							active: year_payload.active,
						},
					},
					payload.supersede ? "supersede" : "update",
				);

				const quarters_payload = payload.quarters;
				for (let i = 0; i < quarters_payload.length; i++) {
					const entry = quarters_payload[i];
					const next = quarters_payload[i + 1];
					await tx.quarter.updateWithWeeks({
						where: { id: entry.id },
						data: {
							name: entry.name,
							start_date: new Date(entry.start_date),
							end_date: new Date(entry.end_date),
							indexing_start: new Date(entry.start_date),
							indexing_end: new Date(next ? next.start_date : entry.end_date),
						},
					});
				}

				return y;
			}),
		);
	}

	DeleteOne(id: number): Observable<TD.Types.ISchoolYear> {
		return from(this.prisma.schoolYear.delete({ where: { id } }));
	}
}
