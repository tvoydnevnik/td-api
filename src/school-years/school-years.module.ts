import { SchoolYearsController } from "./school-years.controller";
import { SchoolYearsService } from "./school-years.service";
import { Module } from "@nestjs/common";

@Module({
	controllers: [SchoolYearsController],
	providers: [SchoolYearsService],
	exports: [SchoolYearsService],
})
export class SchoolYearsModule {}
