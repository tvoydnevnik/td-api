import { SchoolYearsService } from "./school-years.service";
import { mergeMap } from "rxjs";
import {
	Body,
	Controller,
	Delete,
	Get,
	Param,
	Patch,
	Post,
} from "@nestjs/common";
import { TD } from "@td";

@Controller("school-years")
export class SchoolYearsController {
	constructor(private readonly schoolYearsService: SchoolYearsService) {}

	@Get()
	GetAll(): TD.EndpointResponse<TD.Types.ISchoolYear[]> {
		return this.schoolYearsService
			.GetAll()
			.pipe(mergeMap((y) => TD.CreateEndpointResponse(y)));
	}

	@Get("current")
	GetCurrentYear(): TD.EndpointResponse<TD.Types.ISchoolYear> {
		return this.schoolYearsService
			.GetCurrent()
			.pipe(mergeMap((y) => TD.CreateEndpointResponse(y)));
	}

	@Get(":id")
	GetById(@Param("id") id: number): TD.EndpointResponse<TD.Types.ISchoolYear> {
		return this.schoolYearsService
			.GetById(id)
			.pipe(mergeMap((y) => TD.CreateEndpointResponse(y)));
	}

	@Get(":id/quarters")
	GetQuartersByYearId(
		@Param("id") id: number,
	): TD.EndpointResponse<TD.Types.IQuarter[]> {
		return this.schoolYearsService
			.GetQuartersByYearId(id)
			.pipe(mergeMap((r) => TD.CreateEndpointResponse(r)));
	}

	@TD.MinimalRole(TD.Types.UserRole.HEADMASTER)
	@Post()
	CreateOne(
		@Body() body: TD.Types.ISchoolYear,
	): TD.EndpointResponse<TD.Types.ISchoolYear> {
		return this.schoolYearsService
			.CreateOne(body)
			.pipe(mergeMap((y) => TD.CreateEndpointResponse(y)));
	}

	@TD.MinimalRole(TD.Types.UserRole.HEADMASTER)
	@Post("wizard")
	CreateWithWizard(
		@TD.ZodValidateBody({
			throw: true,
			validator: TD.Types.ISchoolYearWizardPayload,
		})
		@Body()
		body: TD.Types.ISchoolYearWizardPayload,
	): TD.EndpointResponse<TD.Types.ISchoolYear> {
		return this.schoolYearsService
			.CreateWithWizard(body)
			.pipe(mergeMap((r) => TD.CreateEndpointResponse(r)));
	}

	@TD.MinimalRole(TD.Types.UserRole.HEADMASTER)
	@Post("supersede")
	Supersede(@Body() body: { with: number }): TD.EndpointResponse<boolean> {
		return this.schoolYearsService
			.Supersede(body.with)
			.pipe(mergeMap((d) => TD.CreateEndpointResponse(d)));
	}

	@TD.MinimalRole(TD.Types.UserRole.HEADMASTER)
	@Patch("wizard/:id")
	UpdateWithWizard(
		@Param("id") id: number,
		@Body() body: TD.Types.IEditSchoolYearWizardPayload,
	): TD.EndpointResponse<TD.Types.ISchoolYear> {
		return this.schoolYearsService
			.UpdateWithWizard(id, body)
			.pipe(mergeMap((r) => TD.CreateEndpointResponse(r)));
	}

	@TD.MinimalRole(TD.Types.UserRole.HEADMASTER)
	@Patch(":id")
	UpdateOne(
		@Param("id") id: number,
		@Body() body: Partial<TD.Types.ISchoolYear>,
	): TD.EndpointResponse<TD.Types.ISchoolYear> {
		return this.schoolYearsService
			.UpdateOne(id, body)
			.pipe(mergeMap((y) => TD.CreateEndpointResponse(y)));
	}

	@TD.MinimalRole(TD.Types.UserRole.HEADMASTER)
	@Delete(":id")
	DeleteOne(
		@Param("id") id: number,
	): TD.EndpointResponse<TD.Types.ISchoolYear> {
		return this.schoolYearsService
			.DeleteOne(id)
			.pipe(mergeMap((res) => TD.CreateEndpointResponse(res)));
	}
}
