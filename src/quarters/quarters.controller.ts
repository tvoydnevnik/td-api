import { QuartersService } from "./quarters.service";
import { mergeMap } from "rxjs";
import {
	Body,
	Controller,
	Delete,
	Get,
	Param,
	Patch,
	Post,
	Query,
} from "@nestjs/common";
import { TD } from "@td";

@Controller("quarters")
export class QuartersController {
	constructor(private readonly quartersService: QuartersService) {}

	@Get()
	GetAll(): TD.EndpointResponse<TD.Types.IQuarter[]> {
		return this.quartersService
			.GetAll()
			.pipe(mergeMap((quarters) => TD.CreateEndpointResponse(quarters)));
	}

	@Get("weeks")
	GetAllWeeks(): TD.EndpointResponse<TD.Types.IQuarterWeek[]> {
		return this.quartersService
			.GetAllWeeks()
			.pipe(mergeMap((weeks) => TD.CreateEndpointResponse(weeks)));
	}

	@Get("current")
	GetCurrent(): TD.EndpointResponse<TD.Types.IQuarter> {
		return this.quartersService
			.GetCurrent()
			.pipe(mergeMap((quarter) => TD.CreateEndpointResponse(quarter)));
	}

	@Get("weeks/current")
	GetCurrentWeek(): TD.EndpointResponse<TD.Types.IQuarterWeek> {
		return this.quartersService
			.GetCurrentWeek()
			.pipe(mergeMap((week) => TD.CreateEndpointResponse(week)));
	}

	@Get("current-year")
	GetCurrentYearQuarters(): TD.EndpointResponse<TD.Types.IQuarter[]> {
		return this.quartersService
			.GetCurrentYearQuarters()
			.pipe(mergeMap((d) => TD.CreateEndpointResponse(d)));
	}

	@Get(":id")
	GetById(@Param("id") id: number): TD.EndpointResponse<TD.Types.IQuarter> {
		return this.quartersService
			.GetById(id)
			.pipe(mergeMap((quarter) => TD.CreateEndpointResponse(quarter)));
	}

	@Get("current/pagination-data")
	GetCurrentPaginationData(): TD.EndpointResponse<TD.Types.IQuarterPagination> {
		return this.quartersService
			.GetCurrentPaginationData()
			.pipe(mergeMap((p) => TD.CreateEndpointResponse(p)));
	}

	@Get(":id/pagination-data")
	GetPaginationData(
		@Param("id") quarter_id: number,
	): TD.EndpointResponse<TD.Types.IQuarterPagination> {
		return this.quartersService
			.GetPaginationData({ quarter_id })
			.pipe(mergeMap((p) => TD.CreateEndpointResponse(p)));
	}

	@Get("weeks/:id")
	GetWeekById(
		@Param("id") id: number,
	): TD.EndpointResponse<TD.Types.IQuarterWeek> {
		return this.quartersService
			.GetWeekById(id)
			.pipe(mergeMap((week) => TD.CreateEndpointResponse(week)));
	}

	@TD.MinimalRole(TD.Types.UserRole.ADMIN)
	@Post()
	Create(
		@Body() body: TD.Types.IQuarter | TD.Types.IQuarter[],
	): TD.EndpointResponse<TD.Types.IQuarter | TD.Types.IQuarter[]> {
		return this.quartersService
			.Create(body)
			.pipe(mergeMap((quarter) => TD.CreateEndpointResponse(quarter)));
	}

	@TD.MinimalRole(TD.Types.UserRole.ADMIN)
	@Post("weeks")
	CreateWeek(
		@Body() body: TD.Types.IQuarterWeek | TD.Types.IQuarterWeek[],
	): TD.EndpointResponse<TD.Types.IQuarterWeek[]> {
		return this.quartersService
			.CreateWeeks(body)
			.pipe(mergeMap((week) => TD.CreateEndpointResponse(week)));
	}

	@TD.MinimalRole(TD.Types.UserRole.ADMIN)
	@Post("weeks/fill-for-quarter/:id")
	FillWeeksForQuarter(
		@Param("id") id: number,
		@Query("force") force = false,
	): TD.EndpointResponse<TD.Types.IQuarterWeek[]> {
		return this.quartersService
			.FillWeeksForQuarter(id, force)
			.pipe(mergeMap((res) => TD.CreateEndpointResponse(res)));
	}

	@TD.MinimalRole(TD.Types.UserRole.ADMIN)
	@Patch(":id")
	UpdateOne(
		@Param("id") id: number,
		@Body() body: Partial<TD.Types.IQuarter>,
	): TD.EndpointResponse<TD.Types.IQuarter> {
		return this.quartersService
			.UpdateOne(id, body)
			.pipe(mergeMap((quarter) => TD.CreateEndpointResponse(quarter)));
	}

	@TD.MinimalRole(TD.Types.UserRole.ADMIN)
	@Patch("weeks/:id")
	UpdateWeek(
		@Param("id") id: number,
		@Body() body: Partial<TD.Types.IQuarterWeek>,
	): TD.EndpointResponse<TD.Types.IQuarterWeek> {
		return this.quartersService
			.UpdateWeek(id, body)
			.pipe(mergeMap((week) => TD.CreateEndpointResponse(week)));
	}

	@TD.MinimalRole(TD.Types.UserRole.ADMIN)
	@Delete(":id")
	DeleteOne(@Param("id") id: number): TD.EndpointResponse<TD.Types.IQuarter> {
		return this.quartersService
			.DeleteOne(id)
			.pipe(mergeMap((res) => TD.CreateEndpointResponse(res)));
	}

	@TD.MinimalRole(TD.Types.UserRole.ADMIN)
	@Delete("weeks/:id")
	DeleteWeek(
		@Param("id") id: number,
	): TD.EndpointResponse<TD.Types.IQuarterWeek> {
		return this.quartersService
			.DeleteWeek(id)
			.pipe(mergeMap((res) => TD.CreateEndpointResponse(res)));
	}
}
