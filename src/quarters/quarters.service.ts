import { Injectable } from "@nestjs/common";

import { forkJoin, from, map, mergeMap, Observable, tap } from "rxjs";

import { PrismaService } from "@td-prisma";
import { Prisma } from "@prisma/client";

import { FillQuarterWeeks } from "./fill-quarter-weeks";
import { TD } from "@td";

@Injectable()
export class QuartersService {
	private readonly _order: Prisma.QuarterOrderByWithRelationInput = {
		start_date: "asc",
	};

	constructor(private readonly prisma: PrismaService) {}

	GetAll(): Observable<TD.Types.IQuarter[]> {
		return from(
			this.prisma.quarter.findMany({
				include: { school_year: true },
				orderBy: { start_date: "asc" },
			}),
		);
	}

	GetAllWeeks(): Observable<TD.Types.IQuarterWeek[]> {
		return from(this.prisma.quarterWeek.findMany({ orderBy: this._order }));
	}

	GetCurrent(): Observable<TD.Types.IQuarter> {
		return from(this.prisma.extended.quarter.findCurrent());
	}

	GetCurrentWeek(): Observable<TD.Types.IQuarterWeek> {
		return from(this.prisma.extended.quarterWeek.findCurrent());
	}

	GetCurrentYearQuarters(): Observable<TD.Types.IQuarter[]> {
		return from(
			this.prisma.quarter.findMany({
				where: { school_year: { active: true } },
				orderBy: this._order,
			}),
		);
	}

	GetById(id: number): Observable<TD.Types.IQuarter> {
		return from(
			this.prisma.quarter.findUnique({
				where: { id },
				include: { school_year: true, weeks: true },
			}),
		).pipe(
			tap((v) => {
				if (!v)
					throw TD.CreateHttpException(
						TD.HTTP_STATUS.NOT_FOUND,
						"Quarter not found",
					);
			}),
		);
	}

	GetCurrentPaginationData(): Observable<TD.Types.IQuarterPagination> {
		return from(this.prisma.extended.quarter.findCurrent()).pipe(
			mergeMap((q) => this.GetPaginationData({ quarter_id: q.id })),
		);
	}

	GetPaginationData(
		request: TD.Types.IQuarterPaginationRequest,
	): Observable<TD.Types.IQuarterPagination> {
		return forkJoin({
			current: this.prisma.extended.quarterWeek.findCurrent({
				where: { quarter_id: request.quarter_id },
			}),
			weeks: this.prisma.quarterWeek.findMany({
				where: { quarter_id: request.quarter_id },
			}),
		}).pipe(
			map(({ current, weeks }) => ({
				current_week: current.index,
				first_week: weeks.at(0)?.index,
				last_week: weeks.at(-1)?.index,
			})),
		);
	}

	GetWeekById(id: number): Observable<TD.Types.IQuarterWeek> {
		return from(this.prisma.quarterWeek.findUnique({ where: { id } })).pipe(
			tap((v) => {
				if (!v)
					throw TD.CreateHttpException(
						TD.HTTP_STATUS.NOT_FOUND,
						"Quarter week not found",
					);
			}),
		);
	}

	Create(
		payload:
			| TD.Types.DeepPartial<TD.Types.IQuarter>
			| Array<TD.Types.DeepPartial<TD.Types.IQuarter>>,
	): Observable<TD.Types.IQuarter[]> {
		const data = Array.isArray(payload) ? payload : [payload];
		return from(
			this.prisma.$transaction(
				data.map((v) =>
					this.prisma.quarter.create({
						data: {
							name: v.name,
							start_date: new Date(v.start_date),
							end_date: new Date(v.end_date),
							indexing_start: new Date(v.indexing_start),
							indexing_end: new Date(v.indexing_end),
							school_year_id: v.school_year?.id,
						},
					}),
				),
			),
		);
	}

	CreateWeeks(
		payload:
			| TD.Types.DeepPartial<TD.Types.IQuarterWeek>
			| Array<TD.Types.DeepPartial<TD.Types.IQuarterWeek>>,
	): Observable<TD.Types.IQuarterWeek[]> {
		const data = Array.isArray(payload) ? payload : [payload];
		return from(
			this.prisma.$transaction(
				data.map((v) =>
					this.prisma.quarterWeek.create({
						data: {
							index: v.index,
							start_date: new Date(v.start_date),
							end_date: new Date(v.end_date),
							indexing_start: new Date(v.indexing_start),
							indexing_end: new Date(v.indexing_end),
							quarter_id: v.quarter?.id,
						},
					}),
				),
			),
		);
	}

	FillWeeksForQuarter(
		id: number,
		force: boolean,
	): Observable<TD.Types.IQuarterWeek[]> {
		return this.GetById(id).pipe(
			mergeMap((quarter) => {
				if (quarter.weeks?.length !== 0 && !force) {
					throw TD.CreateHttpException(
						TD.HTTP_STATUS.CONFLICT,
						"Quarter already has weeks, will not fill",
					);
				}

				return this.prisma.$transaction(async (tx) => {
					const r = await FillQuarterWeeks(tx, quarter);
					return r.created;
				});
			}),
		);
	}

	UpdateOne(
		id: number,
		payload: TD.Types.DeepPartial<TD.Types.IQuarter>,
	): Observable<TD.Types.IQuarter> {
		return from(
			this.prisma.extended.quarter.updateWithWeeks({
				where: { id },
				data: {
					name: payload.name,
					start_date: new Date(payload.start_date),
					end_date: new Date(payload.end_date),
					indexing_start: new Date(payload.indexing_start),
					indexing_end: new Date(payload.indexing_end),
					school_year_id: payload.school_year?.id,
				},
			}),
		);
	}

	UpdateWeek(
		id: number,
		payload: TD.Types.DeepPartial<TD.Types.IQuarterWeek>,
	): Observable<TD.Types.IQuarterWeek> {
		return from(
			this.prisma.quarterWeek.update({
				where: { id },
				data: {
					index: payload.index,
					start_date: new Date(payload.start_date),
					end_date: new Date(payload.end_date),
					indexing_start: new Date(payload.indexing_start),
					indexing_end: new Date(payload.indexing_end),
					quarter_id: payload.quarter?.id,
				},
			}),
		);
	}

	DeleteOne(id: number): Observable<TD.Types.IQuarter> {
		return from(this.prisma.quarter.delete({ where: { id } }));
	}

	DeleteWeek(id: number): Observable<TD.Types.IQuarterWeek> {
		return from(this.prisma.quarterWeek.delete({ where: { id } }));
	}
}
