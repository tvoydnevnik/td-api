import { SchoolYearsModule } from "../school-years/school-years.module";
import { QuartersController } from "./quarters.controller";
import { QuartersService } from "./quarters.service";
import { Module } from "@nestjs/common";

@Module({
	imports: [SchoolYearsModule],
	controllers: [QuartersController],
	providers: [QuartersService],
	exports: [QuartersService],
})
export class QuartersModule {}
