import { Prisma } from "@prisma/client";

import { DateTime } from "luxon";

import { isDefaultPrismaClient } from "@td/type-guards/is-default-prisma-client";
import { TD } from "@td";

const WEEK_INDEX_START = 0;

export async function FillQuarterWeeks(
	client: Prisma.DefaultPrismaClient | Prisma.TransactionClient,
	quarter: TD.Types.IQuarter,
): Promise<{
	created: TD.Types.IQuarterWeek[];
	first_index: number;
	last_index: number;
}> {
	const quarter_start = DateTime.fromJSDate(quarter.start_date);
	const quarter_end = DateTime.fromJSDate(quarter.end_date);

	let iterationDate = quarter_start;
	let weekIndex = WEEK_INDEX_START;
	const quarterWeeks: Prisma.QuarterWeekUncheckedCreateInput[] = [];
	while (iterationDate <= quarter_end) {
		// week ends on saturday, but indexing continues up to sunday
		const week_end = iterationDate.plus({
			days: 7 - iterationDate.weekday - 1,
		});
		const indexing_end = week_end.plus({ days: 1 });

		quarterWeeks.push({
			quarter_id: quarter.id,
			index: weekIndex,
			indexing_start: iterationDate.toJSDate(),
			start_date: iterationDate.toJSDate(),
			indexing_end: indexing_end.toJSDate(),
			end_date: week_end.toJSDate(),
		});

		// jump to the next week, as should already be on the last day on current one
		iterationDate = indexing_end.plus({ days: 1 });
		weekIndex++;
	}
	// last quarter week is indexed the same way its quarter is indexed
	quarterWeeks.at(-1).end_date = quarter_end.toJSDate();
	quarterWeeks.at(-1).indexing_end = quarter_end.toJSDate();

	const upsert_query = (v: Prisma.QuarterWeekUncheckedCreateInput) => ({
		where: {
			unique_quarters_week: { quarter_id: quarter.id, index: v.index },
		},
		create: {
			quarter_id: v.quarter_id,
			index: v.index,
			start_date: v.start_date,
			end_date: v.end_date,
			indexing_start: v.indexing_start,
			indexing_end: v.indexing_end,
		},
		update: {
			start_date: v.start_date,
			end_date: v.end_date,
			indexing_start: v.indexing_start,
			indexing_end: v.indexing_end,
		},
	});

	let weeks = [];
	if (isDefaultPrismaClient(client)) {
		weeks = await client.$transaction(
			quarterWeeks.map((v) => client.quarterWeek.upsert(upsert_query(v))),
		);
	} else {
		for (const entry of quarterWeeks) {
			weeks.push(await client.quarterWeek.upsert(upsert_query(entry)));
		}
	}

	return {
		created: weeks,
		first_index: WEEK_INDEX_START,
		last_index: weekIndex,
	};
}
