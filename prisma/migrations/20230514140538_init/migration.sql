-- CreateTable
CREATE TABLE "bell_schedules" (
    "id" INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,
    "name" VARCHAR,
    "schedule" JSONB NOT NULL,

    CONSTRAINT "bell_schedules_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "contacts" (
    "id" INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,
    "email" VARCHAR,
    "phone" VARCHAR,
    "telegram" VARCHAR,
    "instagram" VARCHAR,
    "www" JSONB,

    CONSTRAINT "contacts_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "diary" (
    "id" INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,
    "timetable_entry_id" INTEGER NOT NULL,
    "student_id" INTEGER NOT NULL,
    "teacher_id" INTEGER,
    "marks" VARCHAR[],
    "comment" VARCHAR,
    "created_at" TIMESTAMPTZ(6) DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMPTZ(6) DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "diary_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "form_lessons" (
    "id" INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,
    "lesson_id" INTEGER NOT NULL,
    "form_id" INTEGER NOT NULL,
    "form_subgroup_id" INTEGER,
    "teacher_id" INTEGER NOT NULL,
    "school_year_id" INTEGER NOT NULL,

    CONSTRAINT "form_lessons_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "form_subgroups" (
    "id" INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,
    "name" VARCHAR NOT NULL,
    "form_id" INTEGER NOT NULL,

    CONSTRAINT "form_subgroups_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "forms" (
    "id" INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,
    "grade" INTEGER NOT NULL,
    "letter" VARCHAR NOT NULL,
    "master_id" INTEGER NOT NULL,
    "graduated" BOOLEAN NOT NULL DEFAULT false,

    CONSTRAINT "forms_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "headmasters" (
    "id" INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,
    "user_id" INTEGER NOT NULL,

    CONSTRAINT "headmasters_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "lessons" (
    "id" INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,
    "name" VARCHAR,

    CONSTRAINT "lessons_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "migrations" (
    "id" INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,
    "timestamp" BIGINT NOT NULL,
    "name" VARCHAR NOT NULL,

    CONSTRAINT "migrations_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "parents" (
    "id" INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,
    "user_id" INTEGER NOT NULL,

    CONSTRAINT "parents_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "quarter_weeks" (
    "id" INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,
    "index" INTEGER NOT NULL,
    "quarter_id" INTEGER NOT NULL,
    "start_date" DATE NOT NULL,
    "end_date" DATE NOT NULL,
    "indexing_start" DATE NOT NULL,
    "indexing_end" DATE NOT NULL,

    CONSTRAINT "quarter_weeks_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "quarterly_marks" (
    "id" INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,
    "student_id" INTEGER NOT NULL,
    "quarter_id" INTEGER NOT NULL,
    "mark" INTEGER NOT NULL,

    CONSTRAINT "quarterly_marks_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "quarterly_signatures" (
    "id" INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,
    "quarter_id" INTEGER NOT NULL,
    "student_id" INTEGER NOT NULL,
    "parent_id" INTEGER NOT NULL,

    CONSTRAINT "quarterly_signatures_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "quarters" (
    "id" INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,
    "name" VARCHAR NOT NULL,
    "school_year_id" INTEGER NOT NULL,
    "start_date" DATE NOT NULL,
    "end_date" DATE NOT NULL,
    "indexing_start" DATE NOT NULL,
    "indexing_end" DATE NOT NULL,

    CONSTRAINT "quarters_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "registration_queue" (
    "id" INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,
    "code" VARCHAR NOT NULL,
    "user_id" INTEGER NOT NULL,

    CONSTRAINT "registration_queue_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "school" (
    "id" INTEGER NOT NULL GENERATED ALWAYS AS (1) STORED,
    "name" VARCHAR NOT NULL,
    "address" VARCHAR,
    "contacts" JSONB,
    "headmaster_id" INTEGER,

    CONSTRAINT "school_pkey" PRIMARY KEY ("id")
);

-- Insert default entry
INSERT INTO "school" VALUES (default, 'School');

-- CreateTable
CREATE TABLE "school_years" (
    "id" INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,
    "start_date" DATE NOT NULL,
    "end_date" DATE NOT NULL,
    "active" BOOLEAN,

    CONSTRAINT "school_years_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "students" (
    "id" INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,
    "user_id" INTEGER NOT NULL,
    "form_id" INTEGER,

    CONSTRAINT "students_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "teachers" (
    "id" INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,
    "user_id" INTEGER NOT NULL,
    "duty" VARCHAR,

    CONSTRAINT "teachers_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "timetable" (
    "id" INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,
    "form_id" INTEGER NOT NULL,
    "quarter_week_id" INTEGER NOT NULL,
    "form_lesson_id" INTEGER NOT NULL,
    "weekday" INTEGER NOT NULL,
    "index" INTEGER NOT NULL,
    "classroom" VARCHAR NOT NULL,
    "start_time" VARCHAR,
    "end_time" VARCHAR,
    "homework" VARCHAR,
    "lesson_topic" VARCHAR,

    CONSTRAINT "timetable_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "users" (
    "id" INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,
    "username" VARCHAR,
    "email" VARCHAR,
    "last_name" VARCHAR NOT NULL,
    "first_name" VARCHAR NOT NULL,
    "patronymic" VARCHAR,
    "password" VARCHAR,
    "role" INTEGER NOT NULL,
    "contacts" JSONB,
    "personal_info" JSONB,
    "is_registered" BOOLEAN NOT NULL DEFAULT false,
    "created_at" TIMESTAMPTZ(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "users_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "weekly_signatures" (
    "id" INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,
    "quarter_week_id" INTEGER NOT NULL,
    "student_id" INTEGER NOT NULL,
    "parent_id" INTEGER NOT NULL,
    "created_at" TIMESTAMPTZ(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "weekly_signatures_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "yearly_marks" (
    "id" INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,
    "student_id" INTEGER NOT NULL,
    "school_year_id" INTEGER NOT NULL,
    "teacher_id" INTEGER NOT NULL,
    "mark" VARCHAR NOT NULL,

    CONSTRAINT "yearly_marks_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "yearly_signatures" (
    "id" INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,
    "school_year_id" INTEGER NOT NULL,
    "student_id" INTEGER NOT NULL,
    "parent_id" INTEGER NOT NULL,
    "created_at" TIMESTAMPTZ(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "yearly_signatures_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "_FormSubgroupToStudent" (
    "A" INTEGER NOT NULL,
    "B" INTEGER NOT NULL
);

-- CreateTable
CREATE TABLE "_ParentToStudent" (
    "A" INTEGER NOT NULL,
    "B" INTEGER NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "diary_teacher_id_timetable_entry_id_student_id_key" ON "diary"("teacher_id", "timetable_entry_id", "student_id");

-- CreateIndex
CREATE UNIQUE INDEX "form_lessons_form_id_lesson_id_teacher_id_key" ON "form_lessons"("form_id", "lesson_id", "teacher_id");

-- CreateIndex
CREATE UNIQUE INDEX "form_lessons_form_id_lesson_id_teacher_id_form_subgroup_id_key" ON "form_lessons"("form_id", "lesson_id", "teacher_id", "form_subgroup_id");

-- CreateIndex
CREATE UNIQUE INDEX "forms_master_id_key" ON "forms"("master_id");

-- CreateIndex
CREATE UNIQUE INDEX "headmaster_user_unique" ON "headmasters"("user_id");

-- CreateIndex
CREATE UNIQUE INDEX "parent_user_unique" ON "parents"("user_id");

-- CreateIndex
CREATE UNIQUE INDEX "registration_queue_code_key" ON "registration_queue"("code");

-- CreateIndex
CREATE UNIQUE INDEX "unique_active_school_year" ON "school_years"("active");

-- CreateIndex
CREATE UNIQUE INDEX "student_user_unique" ON "students"("user_id");

-- CreateIndex
CREATE UNIQUE INDEX "teacher_user_unique" ON "teachers"("user_id");

-- CreateIndex
CREATE UNIQUE INDEX "username_unique" ON "users"("username");

-- CreateIndex
CREATE UNIQUE INDEX "email_unique" ON "users"("email");

-- CreateIndex
CREATE UNIQUE INDEX "_FormSubgroupToStudent_AB_unique" ON "_FormSubgroupToStudent"("A", "B");

-- CreateIndex
CREATE INDEX "_FormSubgroupToStudent_B_index" ON "_FormSubgroupToStudent"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_ParentToStudent_AB_unique" ON "_ParentToStudent"("A", "B");

-- CreateIndex
CREATE INDEX "_ParentToStudent_B_index" ON "_ParentToStudent"("B");

-- AddForeignKey
ALTER TABLE "diary" ADD CONSTRAINT "teachers_fk" FOREIGN KEY ("teacher_id") REFERENCES "teachers"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "diary" ADD CONSTRAINT "students_fk" FOREIGN KEY ("student_id") REFERENCES "students"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "diary" ADD CONSTRAINT "timetable_fk" FOREIGN KEY ("timetable_entry_id") REFERENCES "timetable"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "form_lessons" ADD CONSTRAINT "lessons_fk" FOREIGN KEY ("lesson_id") REFERENCES "lessons"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "form_lessons" ADD CONSTRAINT "teachers_fk" FOREIGN KEY ("teacher_id") REFERENCES "teachers"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "form_lessons" ADD CONSTRAINT "form_subgroups_fk" FOREIGN KEY ("form_subgroup_id") REFERENCES "form_subgroups"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "form_lessons" ADD CONSTRAINT "school_years_fk" FOREIGN KEY ("school_year_id") REFERENCES "school_years"("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "form_lessons" ADD CONSTRAINT "forms_fk" FOREIGN KEY ("form_id") REFERENCES "forms"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "form_subgroups" ADD CONSTRAINT "forms_fk" FOREIGN KEY ("form_id") REFERENCES "forms"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "forms" ADD CONSTRAINT "teachers_fk" FOREIGN KEY ("master_id") REFERENCES "teachers"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "headmasters" ADD CONSTRAINT "users_fk" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "parents" ADD CONSTRAINT "users_fk" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "quarter_weeks" ADD CONSTRAINT "quarters_fk" FOREIGN KEY ("quarter_id") REFERENCES "quarters"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "quarterly_marks" ADD CONSTRAINT "students_fk" FOREIGN KEY ("student_id") REFERENCES "students"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "quarterly_marks" ADD CONSTRAINT "quarters_fk" FOREIGN KEY ("quarter_id") REFERENCES "quarters"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "quarterly_signatures" ADD CONSTRAINT "quarters_fk" FOREIGN KEY ("quarter_id") REFERENCES "quarters"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "quarterly_signatures" ADD CONSTRAINT "students_fk" FOREIGN KEY ("student_id") REFERENCES "students"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "quarterly_signatures" ADD CONSTRAINT "parents_fk" FOREIGN KEY ("parent_id") REFERENCES "parents"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "quarters" ADD CONSTRAINT "school_years_fk" FOREIGN KEY ("school_year_id") REFERENCES "school_years"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "registration_queue" ADD CONSTRAINT "users_fk" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "school" ADD CONSTRAINT "headmasters_fk" FOREIGN KEY ("headmaster_id") REFERENCES "headmasters"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "students" ADD CONSTRAINT "forms_fk" FOREIGN KEY ("form_id") REFERENCES "forms"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "students" ADD CONSTRAINT "users_fk" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "teachers" ADD CONSTRAINT "users_fk" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "timetable" ADD CONSTRAINT "forms_fk" FOREIGN KEY ("form_id") REFERENCES "forms"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "timetable" ADD CONSTRAINT "quarter_weeks_fk" FOREIGN KEY ("quarter_week_id") REFERENCES "quarter_weeks"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "timetable" ADD CONSTRAINT "form_lessons_fk" FOREIGN KEY ("form_lesson_id") REFERENCES "form_lessons"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "weekly_signatures" ADD CONSTRAINT "students_fk" FOREIGN KEY ("student_id") REFERENCES "students"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "weekly_signatures" ADD CONSTRAINT "parents_fk" FOREIGN KEY ("parent_id") REFERENCES "parents"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "weekly_signatures" ADD CONSTRAINT "quarter_weeks_fk" FOREIGN KEY ("quarter_week_id") REFERENCES "quarter_weeks"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "yearly_marks" ADD CONSTRAINT "teachers_fk" FOREIGN KEY ("teacher_id") REFERENCES "teachers"("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "yearly_marks" ADD CONSTRAINT "school_years_fk" FOREIGN KEY ("school_year_id") REFERENCES "school_years"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "yearly_marks" ADD CONSTRAINT "students_fk" FOREIGN KEY ("student_id") REFERENCES "students"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "yearly_signatures" ADD CONSTRAINT "parents_fk" FOREIGN KEY ("parent_id") REFERENCES "parents"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "yearly_signatures" ADD CONSTRAINT "students_fk" FOREIGN KEY ("student_id") REFERENCES "students"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "yearly_signatures" ADD CONSTRAINT "school_years_fk" FOREIGN KEY ("school_year_id") REFERENCES "school_years"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_FormSubgroupToStudent" ADD CONSTRAINT "_FormSubgroupToStudent_A_fkey" FOREIGN KEY ("A") REFERENCES "form_subgroups"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_FormSubgroupToStudent" ADD CONSTRAINT "_FormSubgroupToStudent_B_fkey" FOREIGN KEY ("B") REFERENCES "students"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_ParentToStudent" ADD CONSTRAINT "_ParentToStudent_A_fkey" FOREIGN KEY ("A") REFERENCES "parents"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_ParentToStudent" ADD CONSTRAINT "_ParentToStudent_B_fkey" FOREIGN KEY ("B") REFERENCES "students"("id") ON DELETE CASCADE ON UPDATE CASCADE;
