/*
  Warnings:

  - A unique constraint covering the columns `[index,quarter_id]` on the table `quarter_weeks` will be added. If there are existing duplicate values, this will fail.

*/
-- DropIndex
DROP INDEX "quarter_week_duration";

-- DropIndex
DROP INDEX "quarter_week_indexing_duration";

-- CreateIndex
CREATE UNIQUE INDEX "unique_quarters_week" ON "quarter_weeks"("index", "quarter_id");
