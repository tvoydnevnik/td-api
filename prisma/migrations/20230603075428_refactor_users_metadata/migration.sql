/*
  Warnings:

  - You are about to drop the column `teacher_id` on the `diary` table. All the data in the column will be lost.
  - You are about to drop the column `teacher_id` on the `quarterly_marks` table. All the data in the column will be lost.
  - You are about to drop the column `teacher_id` on the `yearly_marks` table. All the data in the column will be lost.
  - Made the column `created_at` on table `diary` required. This step will fail if there are existing NULL values in that column.
  - Made the column `updated_at` on table `diary` required. This step will fail if there are existing NULL values in that column.
  - Added the required column `updated_at` to the `users` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "diary" DROP CONSTRAINT "teachers_fk";

-- DropForeignKey
ALTER TABLE "quarterly_marks" DROP CONSTRAINT "teachers_fk";

-- DropForeignKey
ALTER TABLE "yearly_marks" DROP CONSTRAINT "teachers_fk";

-- AlterTable
ALTER TABLE "diary" DROP COLUMN "teacher_id",
ADD COLUMN     "user_id" INTEGER,
ALTER COLUMN "created_at" SET NOT NULL,
ALTER COLUMN "updated_at" SET NOT NULL,
ALTER COLUMN "updated_at" DROP DEFAULT,
ALTER COLUMN "updated_at" SET DATA TYPE TIMESTAMP(3);

-- AlterTable
ALTER TABLE "quarterly_marks" DROP COLUMN "teacher_id",
ADD COLUMN     "user_id" INTEGER;

-- AlterTable
ALTER TABLE "users" ADD COLUMN     "updated_at" TIMESTAMP(3) NOT NULL;

-- AlterTable
ALTER TABLE "yearly_marks" DROP COLUMN "teacher_id",
ADD COLUMN     "user_id" INTEGER;

-- AddForeignKey
ALTER TABLE "diary" ADD CONSTRAINT "user_fk" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "quarterly_marks" ADD CONSTRAINT "user_fk" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "yearly_marks" ADD CONSTRAINT "user_fk" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE CASCADE;
