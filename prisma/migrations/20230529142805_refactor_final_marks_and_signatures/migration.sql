/*
  Warnings:

  - A unique constraint covering the columns `[form_lesson_id,student_id,quarter_id]` on the table `quarterly_marks` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[form_lesson_id,student_id,school_year_id]` on the table `yearly_marks` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `form_lesson_id` to the `quarterly_marks` table without a default value. This is not possible if the table is not empty.
  - Added the required column `form_lesson_id` to the `yearly_marks` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "quarterly_marks" ADD COLUMN     "form_lesson_id" INTEGER NOT NULL,
ADD COLUMN     "teacher_id" INTEGER,
ALTER COLUMN "mark" DROP NOT NULL,
ALTER COLUMN "mark" SET DATA TYPE VARCHAR;

-- AlterTable
ALTER TABLE "quarterly_signatures" ADD COLUMN     "created_at" TIMESTAMPTZ(6) NOT NULL DEFAULT CURRENT_TIMESTAMP;

-- AlterTable
ALTER TABLE "yearly_marks" ADD COLUMN     "form_lesson_id" INTEGER NOT NULL,
ALTER COLUMN "teacher_id" DROP NOT NULL,
ALTER COLUMN "mark" DROP NOT NULL;

-- CreateIndex
CREATE UNIQUE INDEX "quarterly_marks_unique" ON "quarterly_marks"("form_lesson_id", "student_id", "quarter_id");

-- CreateIndex
CREATE UNIQUE INDEX "yearly_marks_unique" ON "yearly_marks"("form_lesson_id", "student_id", "school_year_id");

-- AddForeignKey
ALTER TABLE "quarterly_marks" ADD CONSTRAINT "form_lessons_fk" FOREIGN KEY ("form_lesson_id") REFERENCES "form_lessons"("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "quarterly_marks" ADD CONSTRAINT "teachers_fk" FOREIGN KEY ("teacher_id") REFERENCES "teachers"("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "yearly_marks" ADD CONSTRAINT "form_lessons_fk" FOREIGN KEY ("form_lesson_id") REFERENCES "form_lessons"("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- RenameIndex
ALTER INDEX "diary_timetable_entry_id_student_id_key" RENAME TO "diary_entry_unique";
