/*
  Warnings:

  - A unique constraint covering the columns `[timetable_entry_id,student_id]` on the table `diary` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[start_date,end_date]` on the table `quarter_weeks` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[indexing_start,indexing_end]` on the table `quarter_weeks` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[start_date,end_date]` on the table `quarters` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[indexing_start,indexing_end]` on the table `quarters` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[start_date,end_date]` on the table `school_years` will be added. If there are existing duplicate values, this will fail.

*/
-- DropIndex
DROP INDEX "diary_teacher_id_timetable_entry_id_student_id_key";

-- CreateIndex
CREATE UNIQUE INDEX "diary_timetable_entry_id_student_id_key" ON "diary"("timetable_entry_id", "student_id");

-- CreateIndex
CREATE INDEX "quarter_week_index" ON "quarter_weeks"("index");

-- CreateIndex
CREATE UNIQUE INDEX "quarter_week_duration" ON "quarter_weeks"("start_date", "end_date");

-- CreateIndex
CREATE UNIQUE INDEX "quarter_week_indexing_duration" ON "quarter_weeks"("indexing_start", "indexing_end");

-- CreateIndex
CREATE UNIQUE INDEX "quarter_duration" ON "quarters"("start_date", "end_date");

-- CreateIndex
CREATE UNIQUE INDEX "quarter_indexing_duration" ON "quarters"("indexing_start", "indexing_end");

-- CreateIndex
CREATE UNIQUE INDEX "school_year_duration" ON "school_years"("start_date", "end_date");
