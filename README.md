# TvoyDnevnik NestJS Backend

Backend infrastructure to manage data for TvoyDnevnik applications.

## Usage

### To test on your local machine:

First of all, spin up a Docker container if you don't already have your PostgreSQL and Redis ready.

```shell
# Start Compose project
docker compose up -d
```

Then, provide necessary environment variables via `.env` file or directly via exports on UNIX-like OSes

```properties
# - required
## - optional
# Postgres URL
DATABASE_URL=...
# Redis URL
REDIS_URL=...
# Session secret
SESSION_SECRET=...
## Service account username (used by setup script)
TD_SERVICE_USERNAME=username
## Service account password (used by setup script)
TD_SERVICE_PASSWORD=password
```

Afterward, run database migrations:

```shell
# apply Prisma migrations
npx prisma migrate dev
```

If you need to create a service account (which you probably do, otherwise you won't be able to utilize the API as
setting up schools requires at least TEACHER role), run the setup script:

```shell
# Run setup script
# Don't forget to provide TD_SERVICE_USERNAME and TD_SERVICE_PASSWORD
npm run setup
```

After that you're ready to rock. Run the development server as follows:

```shell
# To start a production build
npm start

# To start a development server with change detection
npm run start:dev

# On subsequent launches you can also skip the Docker step and run this script directly
# It will start Docker in the background for you, and then launch NestJS application.
npm run start:local-dev
```
